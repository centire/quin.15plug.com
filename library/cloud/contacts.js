/* Save Facebook, Yahoo and MSN Contacts using cloud sponge*/
    $("#fym").click(function(e){
        $(".step2").removeClass('hide');
        e.preventDefault();   
        $("#fymsn").ajaxForm({
            success: function(data){
                $(".step2").addClass('hide');
                $('html, body').animate({ scrollTop: $(document).height() - $(window).height() }, 1000, function() {
                    $(this).animate({ scrollTop: 0 }, 1000);
                });

                $("#fym_message").html('<div class="alert alert-success" id="notification">'+ data.message +'&nbsp;&nbsp;<a href="javascript:void(0);" class="btn btn-default" id="close_window" >Close Window</a></div>');
                $("a#close_window").click(function(){
                    window.opener = self;
                    window.close();
                });
            }    
        }).submit();
        
    });
