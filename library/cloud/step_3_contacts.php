<html>
  <head>
    <title>Import Contacts - Step 3</title>
  </head>
      <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
          <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
  <style type="text/css">
  .hide{
    display: none;
  }
  .step2{
    left: 50%;
    position: fixed;
    top: 50%;
    z-index: 100000;
    color: #000;
}
#notification {
    text-align: center;    
}
  </style>
  <body>
    <?php 
      require_once 'csimport.php';

      if (array_key_exists('import_id', $_GET)) {
         $import_id = $_GET['import_id'];
         $service   = $_GET['service'];
         $ses       = $_GET['ses'];
        retrieve_contacts($import_id, 2000, $service, $ses);
      }
      function retrieve_contacts($import_id, $timeout, $service, $ses) {
        $contacts_result = CSImport::get_contacts($import_id);
        $contacts = $contacts_result['contacts'];
        $contacts_owner = $contacts_result['contacts_owner'];
        if(!is_null($contacts_owner)) {
          // echo $contacts_owner->name();
        }
        if(!is_null($contacts)) {

        ?> 
          <div style="padding-left: 50px;">
            <div id= "fym_message"></div>
            <div class="step2 hide">Saving Contacts.....</div>
              <h3>Email Addresses:</h3>
              <?php define("PUBLIC_URI" , "http://mastersoftwaretechnologies.com/za/public"); ?>
              <form name="fymsn" action="<?php echo PUBLIC_URI ;?>/users/index/fymsn-contacts/format/html" id="fymsn" method="post">
              <?php
                foreach ($contacts as $key=>$contact) {
                  echo $contact->emails[0]['value'] . "<br>";
                  ?>
                    <input type="hidden" name="fymsncontacts[]" value="<?php echo $contact->emails[0]['value']; ?>">                
                    <input type="hidden" name="service" value="<?php echo $service; ?>">
                    <input type="hidden" name="ses" value="<?php echo $ses; ?>">
                  <?php              
                }
              ?>
              <br />
                 <input type="button"  id="fym" value="Save Contacts" class="btn btn-primary">
              </form>
          </div>
        <?php
        }
      }
    ?>

    <script src="<?php echo PUBLIC_URI; ?>/js/jquery.js" type="text/javascript"></script>
    <script type="text/javascript" src="http://malsup.github.com/jquery.form.js"></script>
    <script type="text/javascript" src="contacts.js"></script>
  </body>
</html>
