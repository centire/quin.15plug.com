<html>
  <head>
    <title>Import Contacts - Step 1</title>
  </head>
  <body>
    <p>
      Click to import your address book from one of the providers below:
      <br />
      <?php 
        if($_GET['srvc'] == 'facebook'){
      ?>
          <a href='index.php?service=facebook' onclick="open_popup('facebook', true);">Facebook!</a>

      <?php
        }elseif($_GET['srvc'] == 'yahoo'){
      ?>
          <a href='index.php?service=yahoo' onclick="return open_popup('yahoo', true);">Yahoo!</a>
      <?php
        }elseif($_GET['srvc'] == 'outlook'){
      ?>
          <a href='index.php?service=windowslive' onclick="return open_popup('outlook', true)">Windows Live</a>
      <?php
        
        }else {


        }

       ?>
      
    </p>

    <script src="prototype.js" type="text/javascript"></script>
    <script type="text/javascript">
      var input_service;
      function show_u_p_fields(service_name) {
        input_service = service_name;
        $('u_p_inputs').show();
        return false;
      }
      function open_popup(service, focus, username, password, url) {
        if (url === undefined) { url = 'popup.php'; }
        url = url + "?service=" + service + "&ses=" + "<?php echo $_GET['ses']; ?>";
        if (username != null) {
          url = url + '&username=' + username + '&password=' + password;
        }
        popup_height = '300';
        popup_width = '500';

        if (service == 'yahoo') {
          popup_height = '500';
          popup_width = '500';
        } else if (service == 'gmail') {
          popup_height = '600';
          popup_width = '987';
        } else if (service == 'windowslive' || service == 'aol') {
          popup_height = '550';
          popup_width = '600';
        } else {
          popup_height = '600';
          popup_width = '987';
        }

        popup = window.open(url, "_popupWindow", 'height='+popup_height+',width='+popup_width+',location=no,menubar=no,resizable=no,status=no,toolbar=no');
        if (focus) {
          popup.focus();
        }
        else {
          window.focus();
        }

        // wait for the popup window to indicate the import_id to start checking for events...
        return (undefined === popup);
      }
    </script>
  </body>
</html>