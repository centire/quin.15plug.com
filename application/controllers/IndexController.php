<?php

class IndexController extends Zend_Controller_Action
{
    protected $_redirector = null;

    public function init() {

        $this->_redirector = $this->_helper->getHelper('Redirector');
    }

    public function homeAction() {

	    $db = Zend_Registry::get( "db" );

	    if ( $this->getRequest()->isGET() ) {
		    $UserSession = new Zend_Session_Namespace( 'UserSession' );

		    $slider       = $db->fetchAll( "select * from media where section=? order by id DESC limit 10 ",
			    array( 'slider' ), 2 );
		    $sitelogo     = $db->fetchAll( "select * from media where section=?", array( 'site-logo' ), 2 );
		    $pages        = $db->fetchAll( "select * from pages where post_type=?", array( 'page' ), 2 );
		    $vendor       = $db->fetchAll( "select * from vendor where public=?", array( '1' ), 2 );
		    $media        = $db->fetchAll( "select * from media", array(), 2 );
		    $venueIdea    = $db->fetchAll( "select * from media where section = 'venueidea'", array(), 2 );
		    $country_city = $db->fetchAll( "select * from  locations ", array(), 2 );


		    if ( isset( $UserSession->userId ) ) {
			    $sql    = 'SELECT * FROM user WHERE id = ?';
			    $result = $db->fetchAll( $sql, $UserSession->userId );

			    //$media = $db->fetchAll('SELECT * FROM media WHERE owner = ?', array($UserSession->userId), 2);
			    $this->view->data = array( 'result'    => $result,
			                               'slider'    => $slider,
			                               'sitelogo'  => $sitelogo,
			                               'pages'     => $pages,
			                               'media'     => $media,
			                               'vendor'    => $vendor,
			                               'venueidea' => $venueIdea
			    );
		    }
		    $this->view->data = array( 'slider'       => $slider,
		                               'sitelogo'     => $sitelogo,
		                               'pages'        => $pages,
		                               'vendor'       => $vendor,
		                               'media'        => $media,
		                               'venueidea'    => $venueIdea,
		                               'country_city' => $country_city
		    );
	    }
    }

	    public function indexAction() {
	    
	   

		    $result = '';

		    //$this->view->data = array();
		    $db = Zend_Registry::get( "db" );

		    if ( $this->getRequest()->isGET() ) {
			    $UserSession = new Zend_Session_Namespace( 'UserSession' );

			    $slider       = $db->fetchAll( "select * from media where section=? order by id DESC limit 10 ",
				    array( 'slider' ), 2 );


			    $sitelogo     = $db->fetchAll( "select * from media where section=?", array( 'site-logo' ), 2 );
			    $pages        = $db->fetchAll( "select * from pages where post_type=?", array( 'page' ), 2 );
			    $vendor       = $db->fetchAll( "select * from vendor where public=?", array( '1' ), 2 );
			    $media        = $db->fetchAll( "select * from media", array(), 2 );
			    $venueIdea    = $db->fetchAll( "select * from media where section = 'venueidea'", array(), 2 );
			    $country_city = $db->fetchAll( "select * from  locations ", array(), 2 );
			    if ( isset( $UserSession->userId ) ) {
				    $sql    = 'SELECT * FROM user WHERE id = ?';
				    $result = $db->fetchAll( $sql, $UserSession->userId );
				}

			    $this->view->data = array( 'slider'       => $slider,
			                               'sitelogo'     => $sitelogo,
			                               'pages'        => $pages,
			                               'vendor'       => $vendor,
			                               'media'        => $media,
			                               'venueidea'    => $venueIdea,
			                               'country_city' => $country_city,
			                               'result' => $result
			    );

		    }
		    
		
            // Get exclusive offers

		    $this->view->data['siteTitle'] = 'quin.15plug.com';

		    $exclusive = $this->getPosts( 14, 5 );
		    
		    if ( $exclusive ) {
			    $this->view->data['exclusive'] = $exclusive;
		    }
            // Get the party posts
		    $theParty = $this->getPosts(8,8);

		    if( $theParty) {
			    $this->view->data['theParty'] = $theParty;
		    }

            // Get trending posts
		    $trending = $this->getPosts(9,3);
		    if( $trending ) {
			    $this->view->data['trending'] = $trending;
		    }

 			$tipsAndIdea = $this->getPosts( 10, 8 );

		    if ( $tipsAndIdea ) {
			    $this->view->data['tipsAndIdea'] = $tipsAndIdea;
		    }

		    $spotLight = $this->getPosts(11,6);

		    if($spotLight) {
			    $this->view->data['spotLight'] = $spotLight;
		    }

		    $doAndDont= $this->getPosts( 12, 4 );

		    if ( $doAndDont ) {
			    $this->view->data['doAndDont'] = $doAndDont;
		    }
			$ideasAndTips= $this->getPosts( 13, 6 );

		    if ( $ideasAndTips ) {
			    $this->view->data['ideasAndTips'] = $ideasAndTips;
		    }
		    
		    

		    $this->_helper->layout->setLayout( 'home' );
    }

	/**
	 * get posts form blog
	 * @param int $catId
	 * @param int $numberOfPost
	 * @param int $postId
	 *
	 * @return array|mixed
	 */

	private function getPosts($catId = 0,$numberOfPost = -1, $postId = 0){

		$mainUrl = 'http://blog.15plug.com/get-all-posts.php?numberposts='.$numberOfPost;
		//$mainUrl = 'http://local.centire.in/quin.15plug.com/blog/get-all-posts.php?numberposts='.$numberOfPost;

		if( $catId ) {
			$mainUrl .= '&catid='.$catId;
		}

		if( $postId ) {
			$mainUrl .= '&postid=' . $postId;
		}
		
		
		// Init curl request
		$curlRequest = curl_init();

		curl_setopt( $curlRequest, CURLOPT_SSL_VERIFYPEER, false );
		// Will return the response, if false it print the response
		curl_setopt( $curlRequest, CURLOPT_RETURNTRANSFER, true );
		
		curl_setopt($curlRequest, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; rv:19.0) Gecko/20100101 Firefox/19.0");
                
                // Set the url
		curl_setopt( $curlRequest, CURLOPT_URL, $mainUrl );

		// Curl exec
		$response = curl_exec( $curlRequest );

		curl_close( $curlRequest );
		
		  
		return json_decode($response);

	}


	public function footerAction() {
		$this->_helper->layout->setLayout( 'footer' );
	}
}