<?php

class Vendor_PaypalController extends Zend_Controller_Action
{

    protected $_redirector = null;
    //@sunny PayPal API Credentials 
    private $API_UserName;
    private $API_Password;
    private $API_Signature;
    private $currencycode;
    private $billingType = "RecurringPayments";
    private $API_Endpoint;
    private $PAYPAL_URL;
    private $token;
    private $version = 86;
    private $allownote = 1;
    private $logimg = 'images/rsz_logo.png'; //'http://mastersoftwaretechnologies.com:50/images/rsz_logo.png';
    private $itemName;
    private $itemNumber;
    private $amount;
    private $quntity;
    private $paymentType = 'Authorization';
    private $custom;
    private $vendorId;
    private $return;
    private $cancelReturn;
    private $method;
    private $parms;
    private $responseToken;
    private $payerId;
    private $shipToName;
    private $shipToStreet;
    private $shipToCity;
    private $shipToState;
    private $shipToZip;
    private $shipToCountry;
    private $amt;
    private $timestamp;
    private $description;
    private $billingperiod;
    private $billingfrequency;  
    private $profileid;
    private $manageAction;
    private $recurringStartdate;
    private $amountRecurring;

    public function init(){
      $this->_redirector = $this->_helper->getHelper('Redirector');
      $db=Zend_Registry::get("db"); 
      $paypal_credentials = $db->fetchAll("SELECT * FROM paypal_credentials");
      if($paypal_credentials){
        $this->API_UserName = $paypal_credentials[0]->api_username;
        $this->API_Password = $paypal_credentials[0]->api_password;
        $this->API_Signature = $paypal_credentials[0]->api_signature;
        $this->currencycode = $paypal_credentials[0]->currency_code;
        $this->PAYPAL_URL = $paypal_credentials[0]->paypal_url."?cmd=_express-checkout&token=";
        $this->API_Endpoint = $paypal_credentials[0]->paypal_api;
      }
    }
    
    public function indexAction(){
        $this->view->msg='';
    }
    

    //@sunny paypal checkout action 
    public function checkoutAction(){
      $request = new Zend_Controller_Request_Http;
      $db=Zend_Registry::get("db"); 
      $paypalSession = new Zend_Session_Namespace('paypal');
      try{

          if($request->isPost()){
            $paypalSession->return = $this->return = $this->getRequest()->getPost('return',null);
            $paypalSession->cancelReturn = $this->cancelReturn = $this->getRequest()->getPost('cancel_return',null); 
            $paypalSession->itemName = $this->itemName = $this->getRequest()->getPost('item_name',null);
            $paypalSession->itemNumber = $this->itemNumber = $this->getRequest()->getPost('item_number',null);
            $paypalSession->custom = $this->custom = $this->getRequest()->getPost('custom',null);
            $paypalSession->vendorId = $this->vendorId = $this->getRequest()->getPost('vendor_id',null);
            $paypalSession->amount = $this->amount = $this->getRequest()->getPost('amount',null);
            $paypalSession->quntity = $this->quntity = $this->getRequest()->getPost('quntity',null);
            $paypalSession->amountRecurring = $this->amountRecurring = $this->getRequest()->getPost('amount',null);
            $this->method = 'SetExpressCheckout';
            $custom = json_decode($this->custom);
            
        
            //@ unsubcribe payment and new plan 
            $paypal_trans = $db->fetchAll("SELECT * FROM paypal_transaction  where email = ? AND txn_type != 'express_checkout'  ORDER BY id DESC LIMIT 1",array($custom[0]->email),2);

            if(count($paypal_trans)){
              if($paypal_trans[0]['cancel_recurring_status']==''){
                if($paypal_trans[0]['txn_type'] == 'subscr_payment' || $paypal_trans[0]['txn_type'] == 'subscr_signup' || $paypal_trans[0]['txn_type'] == 'recurring_payment_profile_created' ){
                
                  $unsubresponse = $this->unsubscriberecurring($paypal_trans[0]['subscr_id'],$paypal_trans[0]['plan'],$custom[0]->plan,$custom[0]->email);
                
                  $paypalSession->custom = $this->custom = $unsubresponse;
                  $plandetails = json_decode($unsubresponse);
                  
                  if(isset($plandetails[0]->amount)){
                    $paypalSession->amount = $this->amount = $plandetails[0]->amount;
                  }

                }       
              }      
            }


            //@ call method for parms 
            $this->parms = $this->setexpressCheckout();
            //@ call method for token
            $this->responseToken = $this->curlpaypaltoken();

            $ack = strtoupper($this->responseToken['ACK']);
            if($ack=="SUCCESS" || $ack=="SUCCESSWITHWARNING")
            {
              $paypalSession->token = $this->token = $this->responseToken["TOKEN"];
              $this->redirectToPaypal();
            }else{
              echo "Try again SetExpressCheckout";
              echo "<pre>";
              print_r($this->responseToken);
            } 
            die();

          }
      }catch (Zend_Exception $e){
        $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
            print_r($data); exit;
      }
    }


    // unsubcribe recurring and upgrade to new plan
    public function unsubscriberecurring($profileid=null,$oldPlan=null,$newPlan=null,$email=null) {
       
        $db = Zend_Registry::get("db");
        $this->profileid = $profileid;
        $this->manageAction = 'cancel';
        //$this->parms = $this->managerecurringPaymentsProfile();
        $oldplan_paypal = $db->fetchAll("SELECT * FROM plans WHERE id=?",array($oldPlan),2);
        $newplan_paypal = $db->fetchAll("SELECT * FROM plans WHERE id=?",array($newPlan),2);

        $this->parms = $this->getrecurringpaymentsprofile();
        $this->responseToken = $this->curlpaypaltoken();
        //echo "<pre>"; print_r($this->responseToken); die;
        $ack = strtoupper($this->responseToken['ACK']);
        if($ack=="SUCCESS" || $ack=="SUCCESSWITHWARNING")
        {
          if($this->responseToken['STATUS']!='Cancelled'){
          
            $current_date = date("Y-m-d H:i:s",strtotime(date('Y-m-d', strtotime('now'))));
            $profile_start_date = date('Y-m-d H:i:s', strtotime ($this->responseToken['PROFILESTARTDATE'] ));
            $next_bill_date = date('Y-m-d H:i:s', strtotime ($this->responseToken['NEXTBILLINGDATE']));

            //current date difference
            //$current_diff = abs(strtotime($current_date)-strtotime($next_bill_date));
            //$current_days_left = floor($current_diff/ (60*60*24)); //floor(($current_diff - floor($current_diff / (365*60*60*24)) * 365*60*60*24 - floor(($current_diff - floor($current_diff / (365*60*60*24)) * 365*60*60*24) / (30*60*60*24))*30*60*60*24)/ (60*60*24));

            $date1=date_create($current_date);
            $date2=date_create($next_bill_date);
            $current_diff = date_diff($date1,$date2);
            $current_days_left = $current_diff->format("%a");

            //die($current_days_left.'---'.$current_date.'---'.$next_bill_date);
            //payment month days
            $date3=date_create($profile_start_date);
            $date4=date_create($next_bill_date);
            $month_diff = date_diff($date3,$date4);
            $month_days = $month_diff->format("%a");
            $month_days = ($month_days==0?30:$month_days); // done for same dates or when value is 0

            //$month_diff = abs(strtotime($profile_start_date)-strtotime($next_bill_date));
            //$month_days = floor($month_diff/ (60*60*24));
            //$month_days = ($month_days==0?30:$month_days);
            // per day price
            $perday_oldprice = $oldplan_paypal[0]['price']; 
            $perday_newprice = $newplan_paypal[0]['price'];

            // calculate 
            
            $calculate_amount =  ($current_days_left * (($perday_newprice - $perday_oldprice) /  $month_days));            
            $calculate_amount =  number_format($calculate_amount, 2);
            $amount = ($calculate_amount>0?$calculate_amount:0.02);
            //die($amount.' -- '.$calculate_amount.' -- '.$current_days_left.' -- '.$perday_newprice.' -- '.$perday_oldprice.' -- '.$month_days );
            //$this->manageAction = 'cancel';
            //$this->parms = $this->managerecurringPaymentsProfile();
            //$this->responseToken = $this->curlpaypaltoken();
            return json_encode(array(array("amount"=>$amount,"plan"=>$newplan_paypal[0]['id'],"days_left"=>$current_days_left,"email"=>$email)));
          }else{
            return json_encode(array(array("plan"=>$newplan_paypal[0]['id'],"email"=>$email)));
          }
        }else{
            echo 'getrecurringpaymentsprofile';
            echo '<pre>';
            print_r($this->responseToken);
            die();     
        }
    }
    // cancel old reccuring payments 
    public function  cancel_old_rec_payments()
    { 
      $db=Zend_Registry::get("db"); 
      $custom = json_decode($_SESSION['paypal']['custom'],2);
      $paypal_ipn_data = $db->fetchAll("SELECT subscr_id FROM paypal_transaction WHERE subscr_id != ? AND email = ? AND cancel_recurring_status = ?",array('',$custom[0]['email'],'') ,2);
      foreach($paypal_ipn_data as $data){
        $this->profileid = $data['subscr_id'];
        $this->manageAction = 'cancel';
        $this->parms = $this->managerecurringPaymentsProfile();
        $this->responseToken = $this->curlpaypaltoken();
      }
                  
    }
    //@sunny method papal return url
    public function paypalthankuAction(){
        $this->cancel_old_rec_payments();
        $paypalSession = new Zend_Session_Namespace('paypal');
        $db=Zend_Registry::get("db"); 
        if(isset($_REQUEST['token'])){
          $token =(isset($_REQUEST['token'])?$_REQUEST['token']:'');
          $payerID=(isset($_REQUEST['PayerID'])?$_REQUEST['PayerID']:'');
          $this->method ='GetExpressCheckoutDetails';
          $this->parms  = $this->getExpressCheckoutDetails($token);
          $this->responseToken = $this->curlpaypaltoken();

   
         $ack = strtoupper($this->responseToken['ACK']);
         if($ack=="SUCCESS" || $ack=="SUCCESSWITHWARNING")
         {
          $custom = json_decode($_SESSION['paypal']['custom']);

           $data= array(
              'vendor_id'=>$_SESSION['paypal']['vendorId'],
              'vendor_email'=>$custom[0]->email,
              'plan_id'=>$custom[0]->plan,
              'itemName'=>$_SESSION['paypal']['itemName'],
              'transaction_checkout_details'=>json_encode($this->responseToken),
              'token'=>$this->responseToken['TOKEN'],
              'payer_id'=>$payerID
           );


          // if(!count($db->fetchAll("select * from paypal_transaction_ipn where vendor_email=? ",array($_SESSION['paypal']['custom']),2))){
              $paypal_transaction = $db->insert("paypal_transaction_ipn",$data);

              $id = $db->lastInsertId('paypal_transaction_ipn');
              $paypalVendor = $db->fetchAll("select * from paypal_transaction_ipn where id=?",array($id),2);
                
              if($paypalVendor){
                    $transaction_checkout_details = json_decode($paypalVendor[0]['transaction_checkout_details']);
                    $custom = $transaction_checkout_details->CUSTOM;
                   
                    // DoExpressCheckoutPayment
                    $this->token = $paypalVendor[0]['token'];
                    $this->email = $transaction_checkout_details->EMAIL;
                    $this->method = 'DoExpressCheckoutPayment';
                    $this->payerId = $paypalVendor[0]['payer_id'];
                    $this->amt = $transaction_checkout_details->AMT;
                    $this->itemName = $transaction_checkout_details->L_PAYMENTREQUEST_0_NAME0;
                    $this->itemNumber = $transaction_checkout_details->L_PAYMENTREQUEST_0_NUMBER0;
                    $this->quntity  = $transaction_checkout_details->L_PAYMENTREQUEST_0_QTY0;
                    $this->amount  = $transaction_checkout_details->AMT;
                    $this->paymentType = 'Sale';
                    $this->custom = $transaction_checkout_details->CUSTOM;
                    $this->parms = $this->doexpresscheckoutpayment();
                    $this->responseToken = $this->curlpaypaltoken();
                    
                   $ack = strtoupper($this->responseToken['ACK']);
                    if($ack=="SUCCESS" || $ack=="SUCCESSWITHWARNING")
                   {  

                       $data = array("transaction_doexpresscheckout"=>json_encode($this->responseToken));
                      $db->update("paypal_transaction_ipn",$data,"id=".$id);
                      $paypal_checkout_details = $db->fetchAll("select * from paypal_transaction_ipn where id=?",array($id),2);
                        
                       if($paypal_checkout_details){
                         $transaction_checkout_details = json_decode($paypal_checkout_details[0]['transaction_checkout_details']);
                         $transaction_doexpresscheckout = json_decode($paypal_checkout_details[0]['transaction_doexpresscheckout']);

                          // CreateRecurringPaymentsProfile
                          $custom_details = json_decode($transaction_checkout_details->CUSTOM);
                          if(isset($custom_details[0]->plan)){
                            $plan_details = $db->fetchAll("SELECT * FROM plans where id=?",array($custom_details[0]->plan),2);
                          }
                          $this->token = $transaction_doexpresscheckout->TOKEN;
                          $this->billingperiod = 'Month';
                          $this->billingfrequency = '1';
                          $this->method = 'CreateRecurringPaymentsProfile';
                          $this->shipToName = $transaction_checkout_details->SHIPTONAME;
                          $this->shipToStreet = $transaction_checkout_details->SHIPTOSTREET;
                          $this->shipToCity = $transaction_checkout_details->SHIPTOCITY;
                          $this->shipToState = $transaction_checkout_details->SHIPTOSTATE;
                          $this->shipToZip = $transaction_checkout_details->SHIPTOZIP;
                          $this->shipToCountry = $transaction_checkout_details->SHIPTOCOUNTRYCODE;
                          $this->amt = (isset($plan_details[0]['price'])?$plan_details[0]['price']:$transaction_checkout_details->AMT);
                          $this->timestamp = $transaction_checkout_details->TIMESTAMP;
                          $this->description = $transaction_checkout_details->FIRSTNAME;
                          $this->itemName = $transaction_checkout_details->L_PAYMENTREQUEST_0_NAME0;
                          $this->itemNumber = $transaction_checkout_details->L_PAYMENTREQUEST_0_NUMBER0;
                          $this->quntity  = $transaction_checkout_details->L_PAYMENTREQUEST_0_QTY0;
                          $this->amount  = (isset($plan_details[0]['price'])?$plan_details[0]['price']:$transaction_checkout_details->AMT);
                          //$this->recurringStartdate =  (isset($custom_details[0]->days_left)?gmdate("Y-m-d\TH:i:s\Z",strtotime(date('Y-m-d', strtotime('+'.$custom_details[0]->days_left.' day')))):gmdate("Y-m-d\TH:i:s\Z",strtotime(date('Y-m-d', strtotime('+1 month')))));
                          $this->recurringStartdate = gmdate("Y-m-d\TH:i:s\Z",strtotime(date('Y-m-d', strtotime('+1 month'))));
                          $this->parms = $this->createRecurringPaymentsProfile();  
                          $this->responseToken = $this->curlpaypaltoken();
                          
                          $ack = strtoupper($this->responseToken['ACK']);
                          if($ack=="SUCCESS" || $ack=="SUCCESSWITHWARNING")
                          {  
                            //echo "<pre>"; print_r($this->responseToken["PROFILEID"]); die("gp;a");
                            $data = array("create_recurring_payments_profile"=>json_encode($this->responseToken),"profile_id"=>$this->responseToken["PROFILEID"]);
                            $db->update("paypal_transaction_ipn",$data,"id=".$id);

                          }else{
                            echo "createRecurringPaymentsProfile";
                            print_r($this->responseToken);
                            die();
                           // $this->parms = $this->createRecurringPaymentsProfile();  
                           // $this->responseToken = $this->curlpaypaltoken();
                          }
                      }
                    } else{
                      echo "doexpresscheckoutpayment";
                      print_r($this->responseToken);
                      die();
                    // $this->parms = $this->doexpresscheckoutpayment();
                    // $this->responseToken = $this->curlpaypaltoken();
                    }     
             }
         //}
        }else{
          echo "getExpressCheckoutDetails";
          print_r($this->responseToken);
          die();
          // $this->parms  = $this->getExpressCheckoutDetails($token);
          // $this->responseToken = $this->curlpaypaltoken();

        }
       $this->_helper->redirector("paypalthanku","paypal","vendor");
      }  
    }
     
    public function getrecurringAction(){
      $this->parms = $this->getrecurringpaymentsprofile();  
      $this->responseToken = $this->curlpaypaltoken();
      echo "<pre>";
      print_r($this->responseToken); die();
    }


    //@sunny method redirect to paypal
    private function  redirectToPaypal(){
      $paypalUrl = $this->PAYPAL_URL . urlencode($this->token);
      header("Location: ".$paypalUrl);
    }

    //@sunny method post value into query string
    private function setexpressCheckout(){
 
         $post_parms =  array(
            'LOCALECODE' => 'us',
            'LANDINGPAGE' => 'Billing',
            'SOLUTIONTYPE' => 'Sole',
            'USER' => $this->API_UserName,
            'PWD' => $this->API_Password,
            'SIGNATURE' => $this->API_Signature,
            'VERSION' => $this->version,
            'ALLOWNOTE' => $this->allownote,
            'LOGOIMG'=> $this->logimg,
            'METHOD' => $this->method,
            'AMT'=> $this->amount,
            'L_BILLINGTYPE0'=> $this->billingType,
            'L_BILLINGAGREEMENTDESCRIPTION0'=>$this->itemName.' $'.$this->amountRecurring,
            'L_PAYMENTREQUEST_0_NAME0' => $this->itemName.' $'.$this->amountRecurring,
            'L_PAYMENTREQUEST_0_NUMBER0' => $this->itemNumber,
            'L_PAYMENTREQUEST_0_AMT0' => $this->amount,
            'L_PAYMENTREQUEST_0_QTY0' => $this->quntity,
            'PAYMENTREQUEST_0_AMT' => $this->amount,
            'PAYMENTREQUEST_0_ITEMAMT'=> $this->amount,
            'PAYMENTREQUEST_0_CUSTOM' => $this->custom,
            'PAYMENTREQUEST_0_PAYMENTACTION0'=> $this->paymentType,
            'PAYMENTREQUEST_0_CURRENCYCODE0'=>$this->currencycode,
            'RETURNURL'=> $this->return,
            'CANCELURL'=> $this->cancelReturn );
            $parms = http_build_query($post_parms);
            return $parms;

    }
     
    //@sunny Get Express CheckoutDetails
    private function getExpressCheckoutDetails($token=null){
      $post_parms = array(
        'USER' => $this->API_UserName,
        'PWD' => $this->API_Password,
        'SIGNATURE' => $this->API_Signature,
        'METHOD' => $this->method,
        'VERSION' => $this->version,
        'TOKEN' => $token );
      $parms = http_build_query($post_parms);
      return $parms;
    }

    //@sunny method create DoExpress Checkout Payment
    private function doexpresscheckoutpayment(){

        $post_parms =array(
            'USER' => $this->API_UserName,
            'PWD' => $this->API_Password,
            'SIGNATURE' => $this->API_Signature,
            'METHOD' => $this->method,
            'VERSION' => $this->version,
            'TOKEN' => $this->token,
            'PAYERID' => $this->payerId,
            'AMT' => $this->amt,
            'PAYMENTACTION'=>$this->paymentType,
            'CUSTOM' => $this->custom,
            'PAYMENTREQUEST_0_PAYMENTACTION' => $this->paymentType,
            'PAYMENTREQUEST_0_CURRENCYCODE' => $this->currencycode,
            'L_PAYMENTREQUEST_0_NAME0' => $this->itemName,
            'L_PAYMENTREQUEST_0_QTY0' => $this->quntity,
            'L_PAYMENTREQUEST_0_AMT0' => $this->amount );

        $parms = http_build_query($post_parms);
        return $parms;
    }


    //@sunny method Create Recurring Payments Profile
    private function createRecurringPaymentsProfile(){
        $post_parms = array(
          'USER' => $this->API_UserName,
          'PWD' => $this->API_Password,
          'SIGNATURE' => $this->API_Signature,
          'TOKEN' => $this->token,
          'PAYERID' => $this->payerId,
          'EMAIL' => $this->email,
          'NAME' => $this->shipToName,
          'STREET' => $this->shipToStreet,
          'CITY' => $this->shipToCity,
          'ZIP' => $this->shipToZip,
          'CUSTOM' => $this->custom,
          'COUNTRY' => $this->shipToCountry,
          'BILLINGPERIOD' => $this->billingperiod,
          'BILLINGFREQUENCY' => $this->billingfrequency,
          'AMT' => $this->amt,
          'CURRENCYCODE' => $this->currencycode,
          'METHOD' => $this->method,
          'VERSION' => $this->version,
          'PROFILESTARTDATE' => $this->recurringStartdate, 
          'DESC' => $this->itemName,
          'L_PAYMENTREQUEST_0_NAME0' => $this->itemName,
          'L_PAYMENTREQUEST_0_QTY0' => $this->quntity,
          'L_PAYMENTREQUEST_0_AMT0' => $this->amount,
          'L_PAYMENTREQUEST_0_ITEMCATEGORYn' => $this->description
          );
        $parms = http_build_query($post_parms);
        return $parms;
    }

    

   

    //@sunny method to perform the get Manage Recurring Payments Profile Status
    private function managerecurringPaymentsProfile(){
      $myfile = fopen("newfile.txt", "w") or die("Unable to open file!");
      $txt = "cancel_all";
      fwrite($myfile, $txt);
      fclose($myfile);
     $post_parms = array(
       "USER"=> $this->API_UserName,
       "PWD"=> $this->API_Password,
       "SIGNATURE"=> $this->API_Signature,
       "VERSION"=>$this->version,
       "NOTIFYURL"=>"http://".$_SERVER['HTTP_HOST']."/vendor/index/ipnhandler",
       "METHOD"=>"ManageRecurringPaymentsProfileStatus",
       "PROFILEID"=> $this->profileid,
       "ACTION"=> $this->manageAction,
       'CUSTOM' => $this->custom,
       "NOTE"=> 'Profile cancelled at store'); 
     $parms = http_build_query($post_parms);
     return $parms;
    }
    
    //@sunny method to perform  the Get Recurring Payments Profile Details
    private function getrecurringpaymentsprofile(){
       $post_parms = array(
        'USER' => $this->API_UserName,
        'PWD' => $this->API_Password,
        'SIGNATURE' => $this->API_Signature,
        'VERSION' => $this->version,
        'METHOD' => 'GetRecurringPaymentsProfileDetails',
        'NOTIFYURL'=>'http://'.$_SERVER['HTTP_HOST'].'/vendor/index/ipnhandler',
        'PROFILEID' =>$this->profileid,//I-P5724YJW872U
        );
       $parms = http_build_query($post_parms);
       return $parms;
     }

    //@sunny method to perform the API call to PayPal using API signature 
    private function curlpaypaltoken(){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->API_Endpoint);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS,$this->parms);
          
        $response = curl_exec($curl);
        parse_str($response,$res_token);
        curl_close($curl);

        return $res_token;
    }


    
}