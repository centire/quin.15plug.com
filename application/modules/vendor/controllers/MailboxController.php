<?php

class Vendor_MailboxController extends Zend_Controller_Action
{
    protected $_redirector = null;
    public function init()
    {
        /* Initialize action controller here */
        $this->_helper->layout->setLayout('v');
        $this->_redirector = $this->_helper->getHelper('Redirector');
    }

    public function indexAction()
    {
        // action body
        $request = new Zend_Controller_Request_Http;
        $db=Zend_Registry::get("db");
        
        // get default session namespace
	Zend_Session::rememberMe(604800); // Week
        $sess =  new Zend_Session_Namespace('VendorSession');
       
        if( !isset( $sess->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , 'default', array() );
        }
        
        if($request->isGet()) {                            
            try{   
                $result = $db->fetchAll("SELECT  a.* FROM messages a INNER JOIN ( SELECT  conv_id, MAX(ID) max_ID FROM messages where sent_to = ? GROUP BY conv_id order by id desc) b ON  a.conv_id = b.conv_id AND a.ID = b.max_ID", array($sess->userId), 2);
                $user = $db->fetchAll("select * from user", array(), 2);
		        $ids = array();
                if(isset($result)){
                    foreach($result as $key=> $data) {
                        $ids[] = $data['by_from'];
                        $user_name = $db->fetchAll("select first_name,last_name from user where id = ?", array($data['by_from']), 2);
                        $result[$key]["by_from_name"]=$user_name[0]['first_name']." ".$user_name[0]['last_name'];
                    }
                }
                $id = implode(",", $ids );
                
		$media = $db->fetchAll("SELECT * FROM media where section='user-avatar' and owner in ($id)", array(), 2);
                if( $result ) {                    
                    $this->view->data = array('messages'=>$result, 'user'=> $user, 'media'=>$media);                    
                } else {                    
                    $this->view->data = NULL;                    
                }            
            } catch (Zend_Db_Adapter_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;        
            } catch (Zend_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
            } 
        }
        //echo "<pre>"; print_r($result); die();
        
    }
    /* Messages Box (New Messsage) Action. */
    public function newAction() {
        $request = new Zend_Controller_Request_Http;
        $db=Zend_Registry::get("db");
	Zend_Session::rememberMe(604800); // Week
        $sess =  new Zend_Session_Namespace('VendorSession');
        
        if( !isset($sess->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , 'default', array() );
        }
         if($request->isGet()) {                
            try{                
                $sql = 'SELECT * from user left join favorites on favorites.user_id=user.id  where favorite_id=?';
	        $result = $db->fetchAll($sql, $sess->userId, 2);
		$admin = "SELECT * from user where role=?";
		$adminres = $db->fetchAll($admin, 10, 2);
		$users = array_merge($adminres, $result);
		//echo "<pre>";
		//print_r($result);die;
                if( $users ) {                    
                    $this->view->data = array('method' => $this->getRequest()->getMethod(), 'users'=> $users);                    
                } else {                    
                    $this->view->data = NULL;                    
                }                         
            } catch (Zend_Db_Adapter_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;        
            } catch (Zend_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
            } 
        }
        
        if($request->isPost()) {
            try{
                if(isset($_POST['select2'])){
                    $comma_separated = implode(",", $_POST['select2']);

                }
		$conversation = array(
                    'name'      =>  $_POST['conversation'],
                    'with_who'  =>  $comma_separated,
                    'by_who'    =>  $sess->userId,
                    'date'      =>  date('Y-m-d H:i:s')
                );
                $convers = $db->insert('conversation', $conversation);
                if( $convers ){
                    $conv_id = $db->lastInsertId('conversation');

                    $data = array(
                        'conv_id' => $conv_id,
                        'sent_to' => $comma_separated,
                        'by_from' => $sess->userId,
                        'message' => $_POST['message'],
                        'date'    => date('Y-m-d H:i:s'),
                        'status' =>  0
                    );
                    $n = $db->insert('messages', $data);
                    if( $n ) {
                        $data = array('method' => $this->getRequest()->getMethod(), 'data' =>false, 'resp'=> 'success');
                        $this->view->data  = $data;
                    } else {
                        $data = array('method' => $this->getRequest()->getMethod(), 'data' =>false, 'resp' => 'error');
                        $this->view->data  = $data;
                    }
                }else {
                    $data = array('method' => $this->getRequest()->getMethod(), 'data' =>false, 'resp' => 'error');
                    $this->view->data  = $data;
                }
            } catch (Zend_Db_Adapter_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
        
            } catch (Zend_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
            }
        }
	
    }
        /* Messages Box (Sent) Action. */
    public function sentAction() {
	$db=Zend_Registry::get("db");
        $sess =  new Zend_Session_Namespace('VendorSession');
        if( !isset($sess->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , 'default', array() );
        }
        
        if($this->getRequest()->isGET()) {
            try {
                if( isset( $sess->userId ) ){
                    $result = $db->fetchAll("SELECT a.* FROM messages a INNER JOIN ( SELECT  conv_id, MAX(ID) max_ID FROM messages where by_from = ? AND status in (0,1) GROUP BY conv_id order by id desc) b ON  a.conv_id = b.conv_id AND a.ID = b.max_ID", array($sess->userId), 2);
		    $ids = array();
                    if( isset($result) ){
                        foreach($result as $key=> $data) {
                            $ids[] = $data['sent_to'];
                        }
                    }
                    $id = implode(",", $ids );
                    $user = $db->fetchAll("select * from user where id in ($id)", array($sess->userId), 2);
                    if( $result ) {                    
                        $this->view->data = array('messages'=>$result, 'user'=> $user);                    
                    } else {                    
                        $this->view->data = NULL;                    
                    }
                }
            } catch (Zend_Db_Adapter_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;        
            } catch (Zend_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
            }     
	}
	
    }

    /* Messages Box (trash) Action. */
    public function trashAction() {
        // action body
        $db=Zend_Registry::get("db");
        $sess =  new Zend_Session_Namespace('VendorSession');
        if( !isset($sess->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , 'default', array() );
        }
        
	if($this->getRequest()->isGET()) {
            try {
                if( isset( $sess->userId ) ){
                    $result = $db->fetchAll( "SELECT * from messages where by_from = ? and status=3", array($sess->userId ), 2);
		    if( $result ) {                    
                        $this->view->data = array( 'messages'=>$result );                    
                    } else {                    
                        $this->view->data = NULL;                    
                    }
                }
            } catch (Zend_Db_Adapter_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;        
            } catch (Zend_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
            }     
	}
    }

    public function viewAction()
    {
        // action body
        $db=Zend_Registry::get("db");
        $sess =  new Zend_Session_Namespace('VendorSession');

        if( !isset($sess->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , 'default', array() );
        }	
     	if($this->getRequest()->isGET()) {
            if(isset($sess->userId)){
                $request = Zend_Controller_Front::getInstance()->getRequest();
                $params = $request->getParams();
                $sql = 'SELECT * FROM user WHERE id = ?';
                $result = $db->fetchAll($sql, $sess->userId);
                $usersection = 'user-avatar';
                $id = $this->getRequest()->get('id');
                $resultMedia = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array( $sess->userId, $usersection ), 2);
                $select = $db->select();
                $select->from(array('m' => 'messages'), array( 'id', 'by_from', 'sent_to', 'message', 'date', 'status', 'conv_id' ))
                       ->joinLeft(array('u' => 'user'), 'u.id = m.by_from WHERE m.conv_id ='.$id .' AND m.status in (0,1)', array('email', 'first_name', 'last_name'));
                $messages = $db->fetchAll($select);
                $messages = json_decode(json_encode($messages), true);
                if( $messages ){
                    $data = array(
                      'status'  => 1
                    );                       
                    $updateMessages = $db->update('messages', $data, 'conv_id ='.$params['id']. ' AND sent_to ='. $sess->userId .' AND status = 0');
                }                      
                if( $resultMedia ){
                    $media = $resultMedia ;
                }else {
                    $media = '';
                }
		
                $this->view->data = array('status' => $result[0]->status, 'data' => $result, 'media' => $media, 'messages'=>$messages);
            }
	}
        
        if($this->getRequest()->isPOST()) {
	    
            try {
                // pretend this is a sophisticated database query
                $data = array(
                            'sent_to' => $_POST['sent_to'],
                            'by_from' => $_POST['by_from'],
                            'message' => $_POST['message'],
                            'conv_id' => $_POST['conv_id'],
                            'date'    => date("Y-m-d H:i:s"),
                        );		   
                $n = $db->insert('messages', $data);
                
                if($n) {
                    $this->_redirector->gotoSimple('view', 'mailbox' , null , array('id' => $_POST['conv_id'], 'to' => $_POST['sent_to']) );
                }
                
            } catch (Exception $e) {
                // handle exceptions yourself
                echo $e;
            }
	}
        
    }
    
    public function delAction() {

	Zend_Session::rememberMe(604800); // Week
        $sess =  new Zend_Session_Namespace('VendorSession');
        if( !isset($sess->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , 'default', array() );
        }
       
        if($this->getRequest()->isGET()) {
            $request = new Zend_Controller_Request_Http;
            try{                
                $db = Zend_Registry::get("db");
                if( $id = $request->get('id') ) {
                    $n = $db->delete( 'messages', 'conv_id = '.$id );
                    if ( $n ) {
			$this->view->data = array('data'=>'Conversation deleted successfully !');
			if($request->get('type') == 'inbox'){
				$urlOptions = array('module'=>'vendor', 'controller'=>'mailbox', 'action'=>'index');
                        	$this->_helper->redirector->gotoRoute($urlOptions);
			}else{
				$urlOptions = array('module'=>'vendor', 'controller'=>'mailbox', 'action'=>'sent');
                        	$this->_helper->redirector->gotoRoute($urlOptions);
			}

                        
                        
                    } else {
                        $this->view->data = array('data'=>'Unable to delete conversation, kindly retry !');
                    }
                }                
            } catch (Exception $e ) {
                $this->view->data = array('data'=>$e);
            }
        }
	
    }
    
    
    public function delpermaAction() {
        Zend_Session::rememberMe(604800); // Week
        if($this->getRequest()->isGET()) {
            $request = new Zend_Controller_Request_Http;
            try{                
                $db=Zend_Registry::get("db");
                if( $id = $request->get('id') ) {
                    $n  = $db->delete('messages', 'id = '.$id);
                    if ( $n ) {
                        $this->view->data = array('data'=>'Message deleted successfully !');
                        $urlOptions = array('module'=>'vendor', 'controller'=>'mailbox', 'action'=>'trash');
                        $this->_helper->redirector->gotoRoute($urlOptions);
                    } else {
                        $this->view->data = array('data'=>'Unable to delete Message, kindly retry !');
                    }
                }                
            } catch ( Exception $e ) {
                $this->view->data = array('data' => $e);
            }
        }
    }    
}

