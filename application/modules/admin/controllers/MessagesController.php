<?php

class Admin_MessagesController extends Zend_Controller_Action
{
    protected $_redirector = null;
    public function init()
    {
        /* Initialize action controller here */
        
        $this->_helper->layout->setLayout('admin');
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('index', 'html')
                    ->addActionContext('parent', 'html')
                    ->addActionContext('remove', 'html')
                    ->initContext();
    }

    public function indexAction()
    {
        
        // get default session namespace
	//Zend_Session::rememberMe(604800); // Week
        $sess = new Zend_Session_Namespace('Default');
        
        // action body
        $request = new Zend_Controller_Request_Http;
        $db=Zend_Registry::get("db");
       
        if( !isset($sess->user ) ){
            $this->_redirector->gotoSimple('index', 'login' , null );
        }
        
        if($request->isGet()) {                            
            try{   
                // $result = $db->fetchAll("select * from messages where sent_to=? and id In (Select max(id) From messages Group By conv_id) group by conv_id ORDER BY id DESC", array($sess->user), 2);
                $result = $db->fetchAll("SELECT  a.* FROM    messages a INNER JOIN ( SELECT  conv_id, MAX(ID) max_ID FROM messages where sent_to = ? GROUP BY conv_id order by id desc) b ON  a.conv_id = b.conv_id AND a.ID = b.max_ID", array($sess->user), 2);

                $user = $db->fetchAll("select * from user", array(), 2);
                $ids = array();
                if(isset($result)){
                    foreach($result as $key=> $data) {
                        $ids[] = $data['by_from'];
                    }
                }
		if(count($ids) > 1){
			$id = substr(implode(",", $ids ), 0, -1);
		}else{
			$id = implode(",", $ids );
		}
		
                $media = $db->fetchAll("SELECT * FROM media where section='user-avatar' and owner in ($id)", array(), 2);
 
                if( $result ) {    
              
                    $this->view->data = array('messages'=>$result, 'user'=> $user, 'media'=>$media);                    
                } else { 
                 
                    $this->view->data = NULL;                    
                }            
            } catch (Zend_Db_Adapter_Exception $e) {
                // perhaps a failed login credential, or perhaps the RDBMS is not running
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;        
            } catch (Zend_Exception $e) {
                // perhaps factory() failed to load the specified Adapter class
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
            } 
        }
        
    }
    
    /* Messages Box (New Messsage) Action. */
    public function newAction() {
        $request = new Zend_Controller_Request_Http;
        $db=Zend_Registry::get("db");
	Zend_Session::rememberMe(604800); // Week
        $sess = new Zend_Session_Namespace('Default');
        
        if( !isset($sess->user ) ){
            $this->_redirector->gotoSimple('index', 'login' , null );
        }
         if($request->isGet()) {                
            try{                
                $users = $db->fetchAll("select * from user");

                if( $users ) {                    
                    $this->view->data = array('method' => $this->getRequest()->getMethod(), 'users'=> $users);                    
                } else {                    
                    $this->view->data = NULL;                    
                }            
            } catch (Zend_Db_Adapter_Exception $e) {
                // perhaps a failed login credential, or perhaps the RDBMS is not running
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;        
            } catch (Zend_Exception $e) {
                // perhaps factory() failed to load the specified Adapter class
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
            } 
        }
        
        if($request->isPost()) {
            try{
                if(isset($_POST['select2'])){
                    $comma_separated = implode(",", $_POST['select2']);
                }
                $conversation = array(
                    'name'      =>  $_POST['conversation'],
                    'with_who'  =>  $comma_separated,
                    'by_who'    =>  $sess->user,
                    'date'      =>  date('Y-m-d H:i:s')
                );
                $convers = $db->insert('conversation', $conversation);
                if( $convers ){
                    $conv_id = $db->lastInsertId('conversation');
                    $data = array(
                        'conv_id' => $conv_id,
                        'sent_to' => $comma_separated,
                        'by_from' => $sess->user,
                        'message' => $_POST['message'],
                        'date'    => date('Y-m-d H:i:s'),
                        'status' =>  0
                    );
                    $n = $db->insert('messages', $data);
                    if( $n ) {
                        $data = array('method' => $this->getRequest()->getMethod(), 'data' =>false, 'resp'=> 'success');
                        $this->view->data  = $data;
                    } else {
                        $data = array('method' => $this->getRequest()->getMethod(), 'data' =>false, 'resp' => 'error');
                        $this->view->data  = $data;
                    }
                }else {
                    $data = array('method' => $this->getRequest()->getMethod(), 'data' =>false, 'resp' => 'error');
                    $this->view->data  = $data;
                }
                
                
            } catch (Zend_Db_Adapter_Exception $e) {
                // perhaps a failed login credential, or perhaps the RDBMS is not running
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
        
            } catch (Zend_Exception $e) {
                // perhaps factory() failed to load the specified Adapter class
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
            }
        }
	
    }

    /* Messages Box (Sent) Action. */
    public function sentAction() {
	$db=Zend_Registry::get("db");
	$sess = new Zend_Session_Namespace('Default');
	if( !isset($sess->user ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
        
        if($this->getRequest()->isGET()) {
            try {
                if( isset( $sess->user ) ){
                    $result = $db->fetchAll("SELECT  a.* FROM    messages a INNER JOIN ( SELECT  conv_id, MAX(ID) max_ID FROM messages where by_from = ? AND status in (0,1) GROUP BY conv_id order by id desc) b ON  a.conv_id = b.conv_id AND a.ID = b.max_ID", array($sess->user), 2);
                    $ids = array();
                    if( isset($result) ){
                        foreach($result as $key=> $data) {
                            $ids[] = $data['sent_to'];
                        }
                    }
                    $id = implode(",", $ids );
                    $user = $db->fetchAll("select * from user where id in ($id)", array($sess->user), 2);
                    if( $result ) {                    
                        $this->view->data = array('messages'=>$result, 'user'=> $user);                    
                    } else {                    
                        $this->view->data = NULL;                    
                    }
                }
            } catch (Zend_Db_Adapter_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;        
            } catch (Zend_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
            }     
	}
	
    }

    /* Messages Box (trash) Action. */
    public function trashAction() {
        // action body
        $db=Zend_Registry::get("db");
	$sess = new Zend_Session_Namespace('Default');
	
        if( !isset($sess->user ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
        
	if($this->getRequest()->isGET()) {
            try {
                if( isset( $sess->user ) ){
                    $result = $db->fetchAll("SELECT * from messages where by_from = ? and status=3", array($sess->user), 2);
                    if( $result ) {                    
                        $this->view->data = array( 'messages'=>$result );                    
                    } else {                    
                        $this->view->data = NULL;                    
                    }
                }
            } catch (Zend_Db_Adapter_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;        
            } catch (Zend_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
            }     
	}
    }
    
    public function viewAction()
    {
        // action body
        $db=Zend_Registry::get("db");
	$sess = new Zend_Session_Namespace('Default');
	if( !isset( $sess->user ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}	
     	if($this->getRequest()->isGET()) {
            if(isset($sess->user)){
                $request = Zend_Controller_Front::getInstance()->getRequest();
                $params = $request->getParams();
                $sql = 'SELECT * FROM user WHERE id = ?';
                $result = $db->fetchAll($sql, $sess->user);
                $usersection = 'user-avatar';
                $id = $this->getRequest()->get('id');
                $resultMedia = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array( $sess->user, $usersection ), 2);
                // $messages = $db->fetchAll("SELECT * FROM messages WHERE conv_id=?", array( $id), 2);
                $select = $db->select();
                
                $select->from(array('m' => 'messages'), array( 'id', 'by_from', 'sent_to', 'message', 'date', 'status', 'conv_id' ))
                       ->joinLeft(array('u' => 'user'), 'u.id = m.by_from WHERE m.conv_id ='.$id .' AND m.status in (0,1)', array('email', 'first_name', 'last_name'));
                $messages = $db->fetchAll($select);
                $messages = json_decode(json_encode($messages), true);
                if( $messages ){
                    $data = array(
                      'status'  => 1
                    );                       
                    $updateMessages = $db->update('messages', $data, 'conv_id ='.$params['id']. ' AND sent_to ='. $sess->user .' AND status = 0');
                }                      
                if( $resultMedia ){
                    $media = $resultMedia ;
                }else {
                    $media = '';
                }
                $this->view->data = array('status' => $result[0]->status, 'data' => $result, 'media' => $media, 'messages'=>$messages);
            }
	}
        
        if($this->getRequest()->isPOST()) {
	    
            try {
                // pretend this is a sophisticated database query
                $data = array(
                            'sent_to' => $_POST['sent_to'],
                            'by_from' => $_POST['by_from'],
                            'message' => $_POST['message'],
                            'conv_id' => $_POST['conv_id'],
                            'date'    => date("Y-m-d H:i:s"),
                        );		   
                $n = $db->insert('messages', $data);
                
                if($n) {
                    $this->_redirector->gotoSimple('view', 'messages' , null , array('id' => $_POST['conv_id'], 'to' => $_POST['sent_to']) );
                }
                
            } catch (Exception $e) {
                // handle exceptions yourself
                echo $e;
            }
	}
        
    }
    
    // @mssjeevan support profile
    public function delAction() {
	// get default session namespace
	Zend_Session::rememberMe(604800); // Week
        $sess = new Zend_Session_Namespace('Default');
	if( !isset($sess->user ) ){
            $urlOptions = array('module'=>'admin', 'controller'=>'login', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }
        
        if($this->getRequest()->isGET()) {
            $request = new Zend_Controller_Request_Http;
	    // pretend this is a sophisticated database query
            try{                
                $db=Zend_Registry::get("db");
                if( $id = $request->get('id') ) {
                    $n = $db->delete('messages', 'conv_id = '.$id.'');
                    
                    if ( $n ) {
                        $this->view->data = array('data'=>'Conversation deleted successfully !');
			if($request->get('type') == 'sent'){
		                $urlOptions = array('module'=>'admin', 'controller'=>'messages', 'action'=>'sent');
		                $this->_helper->redirector->gotoRoute($urlOptions);
			}else{
				$urlOptions = array('module'=>'admin', 'controller'=>'messages', 'action'=>'index');
		                $this->_helper->redirector->gotoRoute($urlOptions);
			}
                    } else {
                        $this->view->data = array('data'=>'Unable to delete conversation, kindly retry !');
                    }
                }                
            } catch (Exception $e ) {
                $this->view->data = array('data'=>$e);
            }
        }
	
    }
    
    public function removeAction() {
        Zend_Session::rememberMe(604800); // Week
        $sess = new Zend_Session_Namespace('Default');
        if($this->getRequest()->isGET()) {
            $request = new Zend_Controller_Request_Http;
            try{                
                $db=Zend_Registry::get("db");
                if( $id = $request->get('id') ) {
                    
                    $data = array( 'status' => 3 );                       
                    $n = $db->update('messages', $data, 'id ='.$id.'');
                    if ( $n ) {
                        $this->_helper->json(
                                     array( 'message' => 'Message deleted Successfully !! ' ,
                                            'resp'    => 'success'
                                    )
                        );
                        return;
                    } else {
                        $this->_helper->json(
                                     array( 'message' => 'Unable to delete conversation, kindly retry ! ' ,
                                            'resp'    => 'success'
                                    )
                        );
                        return;
                    }
                }                
            } catch ( Exception $e ) {
                $this->view->data = array('data' => $e);
            }
        }
    }
    
    
    public function delpermaAction() {
        Zend_Session::rememberMe(604800); // Week
        $sess = new Zend_Session_Namespace('Default');
        if($this->getRequest()->isGET()) {
            $request = new Zend_Controller_Request_Http;
            try{                
                $db=Zend_Registry::get("db");
                
                if( $id = $request->get('id') ) {
                    $n  = $db->delete('messages', 'id = '.$id);
                    if ( $n ) {
                        $this->view->data = array('data'=>'Message deleted successfully !');
                        $urlOptions = array('module'=>'admin', 'controller'=>'messages', 'action'=>'trash');
                        $this->_helper->redirector->gotoRoute($urlOptions);
                    } else {
                        $this->view->data = array('data'=>'Unable to delete Message, kindly retry !');
                    }
                }                
            } catch ( Exception $e ) {
                $this->view->data = array('data' => $e);
            }
        }
    }

}

