<?php

class Admin_ResetauthController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        $this->_helper->layout->setLayout('adminlogin');
    }

    public function indexAction()
    {
        $request = new Zend_Controller_Request_Http;
        if ( $request->isGet() ) {
            // display login form    
        }
        
        // action body
        if ( $request->isPost() ) {
            try {
                $email = $this->getRequest()->getPost('email', null);
                $db=Zend_Registry::get("db");
                $result = $db->fetchAll("SELECT * FROM user where email=? and role=10 and admin=1", array($email), 2);
                if ( $result ) {
                    $password_activation_key = rand();
                    $data = array(
                        'password_activation_key'      => $password_activation_key
                    );     
                    $n = $db->update('user', $data, 'id = '.$result[0]['id'].'');
                    if ($n) {
                        //echo "Database updated"; die;
                        $tr = new Zend_Mail_Transport_Sendmail('-freturn_to_me@example.com');
                        Zend_Mail::setDefaultTransport($tr);
                        $mail = new Zend_Mail();
                        $mail->setBodyHtml('Your Password Reset code for quinceanera is:'.$password_activation_key.
                                        '<br/> To reset your password, <a href ="'.PUBLIC_URI.'/admin/forgot-password" target="_blank"> Click Here </a>');
                        $mail->setFrom('admin.quinceanera@gmail.com', 'Email from quinceanera.');
                        $mail->addTo($email, '');
                        $mail->setSubject('quinceanera password reset');
                        $mail->send();
                        echo "<div id='notification' class='alert alert-danger'>";
                            echo $result[0]['email'].", a password reset link and passcode has been sent to you in email.";
                        echo "</div>";
                    } else {
                        echo "<div id='notification' class='alert alert-danger'>";
                            echo mysql_error();
                        echo "</div>";
                    }
                } else {
                    echo "<div id='notification' class='alert alert-danger'>";
                        echo "User with this email does not exist !!!";
                    echo "</div>";
                    
                }
            } catch (Exception $e) {
                // handle exceptions yourself
                echo $e;
            }
        }
    }


}

