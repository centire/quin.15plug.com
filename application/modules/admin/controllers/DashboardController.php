<?php

class Admin_DashboardController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        $this->_helper->layout->setLayout('admin');
    }

    public function indexAction()
    {
        
    }

     
    public function paypalAction()
    {
    		$db=Zend_Registry::get("db");
        	$sess = new Zend_Session_Namespace('Default');
        	$request = new Zend_Controller_Request_Http;
        	if( !isset($sess->user ) ) {
            	$this->_redirector->gotoSimple('index', 'login' , null );
        	}

        	try {
        	   $paypal_cre = $db->fetchAll("SELECT * FROM paypal_credentials ");
               if($request->isGet()){
        			$this->view->data = array("paypal"=>$paypal_cre,"msg"=>""); 
        		}
        		if($request->isPost()){
        		   $where  = $this->getRequest()->getParam('id');
                   $data = $this->getRequest()->getPost();
                   $credentials_update = $db->update('paypal_credentials',$data,'id='.$where);
                   $paypal_cre = $db->fetchAll("SELECT * FROM paypal_credentials where id=$where");
                   if($credentials_update){
                   		$this->view->data = array("paypal"=>$paypal_cre,"msg"=>"Updated"); 
                   }
                   else{

                   	   $this->view->data = array("paypal"=>$paypal_cre,"msg"=>""); 
                   }
                }
                
             
               
        	} catch (Exception $e) {
        		print_r($e); 
        	}
	
    }


}

