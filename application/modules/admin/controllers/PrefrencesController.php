<?php

class Admin_PrefrencesController extends Zend_Controller_Action
{
    protected $_redirector = null;
    public function init()
    {
        /* Initialize action controller here */
        $this->_helper->layout->setLayout('admin');
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
	$ajaxContext->addActionContext('ov', 'html')
	            ->addActionContext('oe', 'html')
		    ->addActionContext('od', 'html')
	            ->initContext();
    }

    public function indexAction()
    {
        // action body
        Zend_Session::rememberMe(604800); // Week
        $sess = new Zend_Session_Namespace('Default');
	if( !isset($sess->user ) ){
            $urlOptions = array('module'=>'admin', 'controller'=>'login', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }
        
        if($this->getRequest()->isGET()) {
	   
		try {
		   
		    $db=Zend_Registry::get("db");
		    $result = $db->fetchAll("SELECT * FROM prefrences where section=?", array('site'), 2);
                    $media = $db->fetchAll("SELECT * FROM media where section=?", array('site-logo'), 2); //echo "<pre>";  print_r($media);die;
		    if ( !$result ) {
		        print("User with this email and password does not exist !");
		    } else {
		        $this->view->data = array('preferences'=>$result, 'media'=>$media);
		    } 
		    
		} catch (Exception $e) {
		    // handle exceptions yourself
		    echo $e;
		}
	}
       
        if($this->getRequest()->isPOST()) {	 
		
		$db=Zend_Registry::get("db");
		// pretend this is a sophisticated database query
    		$data = array(
                                'title' => $_POST['site-title'],
                                'keywords' => $_POST['site-keywords'],
				'description' => $_POST['site-description'],
				'tagline' => $_POST['site-tagline'],
                                'section' => $_POST['site'] 
                            );        
		try {
			
                    $section = $_POST['site'];
		    $query = $db->fetchAll("SELECT * FROM prefrences where section=?", array($section), 2);
		    if ( $query ) {
                        $n = $db->update('prefrences', $data, 'section="'.$section.'"'); // 0 is the section id of site preferences
		    } else {
                        $n = $db->insert('prefrences', $data);
		    }
                    $result = $db->fetchAll("SELECT * FROM prefrences where section=?", array($section), 2);
		    if ( !$result ) {
		        print("User with this email and password does not exist !");
		    } else {
		        $this->view->data = array('preferences'=>$result, 'media'=>$media);
		    }
		    
		} catch (Exception $e) {
		    // handle exceptions yourself
		    echo $e;
		}  
	}
    }


}

