<?php

class Admin_ForgotPasswordController extends Zend_Controller_Action
{
    protected $_redirector = null;
    public function init()
    {
        /* Initialize action controller here */
        $this->_helper->layout->setLayout('adminlogin');
        $this->_redirector = $this->_helper->getHelper('Redirector');
    }

    public function indexAction()
    {
        // action body
        $request = new Zend_Controller_Request_Http;
        if ( $request->isGet() ) {
            // display login form    
        }
        
        // action body
        if ( $request->isPost() ) {
            try {
                $passcode = $this->getRequest()->getPost('passcode', null);
                $pass = md5($this->getRequest()->getPost('password', null));
                $cpass = md5($this->getRequest()->getPost('cpassword', null));
                if ($passcode && $pass && $cpass) {
                    
                    if ($pass===$cpass)
                    {
                        $db=Zend_Registry::get("db");
                        $result = $db->fetchAll("SELECT * FROM user where password_activation_key=? and role=10 and admin=1", array($passcode), 2);
                        if ( $result ) {
                            $password_activation_key = rand();
                            $data = array(
                                'password'      => $cpass,
                                'password_activation_key' => ''
                            );     
                            $n = $db->update('user', $data, 'id = '.$result[0]['id'].'');
                            if ($n) {
                                //echo "Database updated"; die;
                                $tr = new Zend_Mail_Transport_Sendmail('-freturn_to_me@example.com');
                                Zend_Mail::setDefaultTransport($tr);
                                $mail = new Zend_Mail();
                                $mail->setBodyHtml('Your password has been reset successfully, passcode key consumed !!!');
                                $mail->setFrom('admin.quinceanera@gmail.com', 'Email from quinceanera.');
                                $mail->addTo($result[0]['email'], '');
                                $mail->setSubject('quinceanera password updated');
                                $mail->send();
                                echo "<div id='notification' class='alert alert-danger'>";
                                    echo "Password updated successfully. <a href='".PUBLIC_URI."/admin/login'>Click here to login</a>";
                                echo "</div>";
                            } else {
                                echo "<div id='notification' class='alert alert-danger'>";
                                    echo mysql_error();
                                echo "</div>";
                            }
                        } else {
                            echo "<div id='notification' class='alert alert-danger'>";
                                echo "Passcode key is consumed already, please <a href='".PUBLIC_URI."/admin/resetauth'>click on resend passcode and reset link</a> to get new passcode.";
                            echo "</div>";
                        } 
                    } else {
                        echo "<div id='notification' class='alert alert-danger'>";
                            echo "password mismatch please try again !!!";
                        echo "</div>";
                    }
                    
                } else {
                    echo "<div id='notification' class='alert alert-danger'>";
                        echo "All fields are required !!!";
                    echo "</div>";
                }
            } catch (Exception $e) {
                // handle exceptions yourself
                echo $e;
            }
        }
    }


}

