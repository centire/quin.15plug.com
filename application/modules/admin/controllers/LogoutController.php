<?php

class Admin_LogoutController extends Zend_Controller_Action
{
    protected $_redirector = null;
    public function init()
    {
        /* Initialize action controller here */
	$this->_helper->layout->setLayout('admin');
	$this->_redirector = $this->_helper->getHelper('Redirector');
    }

    public function indexAction()
    {
        // action body
	// check if user is not logged in
	Zend_Session::rememberMe(604800); // Week
        $sess = new Zend_Session_Namespace('Default');
	if( isset($sess->user ) ){
	    unset($sess->user);
        }
    $this->_helper->redirector('index','index','index');
	//$this->redirect('/admin/');
	//$this->_redirector->gotoSimple('index', 'index', null );
    }


}

