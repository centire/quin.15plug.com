<?php

class Pages_IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
	$this->_helper->layout->setLayout('pages');
	$this->_redirector = $this->_helper->getHelper('Redirector');
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
	$ajaxContext->addActionContext('avp', 'html')
	            ->initContext();
    }

    public function indexAction()
    {
        // action body
    }
    
    public function viewAction()
    {
        $db=Zend_Registry::get("db");
	
	// create zend object to handle the main http request
        $request = new Zend_Controller_Request_Http;
	
	// get the list of all parameters in query string
	$params = Zend_Controller_Front::getInstance()->getRequest()->getParams();
        
        // action body
        if( $params['id'] ) {            
            // fetch all content of the page from database of the page with parameter in query string
            $content = $db->fetchAll("select * from wp_posts where ID=? and post_type=?", array($params['id'],  $params['type'] ), 2);
            
            if( $content ) {
                // forward content to content page
                $this->view->data = array('content'=>$content);
            } else {
                echo "Invalid url request !!"; exit;
            }
        } else {
            $this->_redirector->gotoSimple('index', 'index' , null );
        }
    }
 public function vendorcatAction(){
	  $db=Zend_Registry::get("db");
	// create zend object to handle the main http request
        $request = new Zend_Controller_Request_Http;
	
	
	// get the list of all parameters in query string
	$params = Zend_Controller_Front::getInstance()->getRequest()->getParams();
        // action body
	if($params['cat'] ) {
	    $catid=$params['cat'];
	    
	    if($params['cat'] == 'all'){
		$catcantent = $db->fetchAll("SELECT * FROM vendor where public='1' ");
	    }else{
		$catcantent = $db->fetchAll( "SELECT * FROM `vendor` WHERE `category`=$catid and public=1" );
	    } //print_r($catcantent);die;
            if( $catcantent) {
                // forward content to content page
                $this->view->data = array('content'=>$catcantent);
            } else {
		$catcantent="No data";
                $this->view->data = array('content'=>$catcantent);
            }
	    
	}else {
            $this->_redirector->gotoSimple('index', 'index' , null );
        }
	
    }
}

