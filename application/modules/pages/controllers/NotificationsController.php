<?php

class Pages_NotificationsController extends Zend_Controller_Action
{
	public function init()
   	{
		/* Initialize action controller here */
		$this->_helper->layout->setLayout('admin');
		$this->_redirector = $this->_helper->getHelper('Redirector');
		$ajaxContext = $this->_helper->getHelper('AjaxContext');
		$ajaxContext->addActionContext('index', 'html')
		            ->initContext();
    	}

	//@ Deepak notifications action
	public function indexAction()
	{
        	$request = new Zend_Controller_Request_Http;
		$params = Zend_Controller_Front::getInstance()->getRequest()->getParams();
		$types = array('job','apply','news','message','image','video');
		if(in_array( $params['type'], $types ) ){ 
		 
			switch($params['type'])
			{
				//case for posting jobs by users
				case "job": 
					$this->email_to_all_related_vendors_post($params['id']);
					break;

				//case for applying jobs by vendor
				case "apply":
					$this->email_to_all_related_vendors_apply($params['id']);
					break;

				//case for updating news by admin
				case "news":
					$this->email_to_all_users();
					break;

				//case for messages b/w referer and referred
				case "message":
					$this->email_to_referer_referred($params['id']);
					break;

				//case for uploading image by vendor			
				case "image":
					$this->email_to_all_related_users_image($params['id'], $params['section']);
					break;

				//case for uploading video by vendor
				case "video":
					$this->email_to_all_related_users_video($params['id'], $params['section']);
					break;
			}		
		}
	} 

	//@ Deepak function for posting job by user
	public function email_to_all_related_vendors_post($job_id)
	{		
		$db=Zend_Registry::get("db");
		$SessDefault = new Zend_Session_Namespace('default');
		$SessVendor = new Zend_Session_Namespace('VendorSession');
		$SessUser = new Zend_Session_Namespace('UserSession');

		$job = 'SELECT * from job where id = ?';
		$result = $db->fetchAll($job, array($job_id), 2); 
		if($result){
			$vendors = $db->fetchAll('SELECT * FROM vendor where category = ? ', array($result[0]['category']), 2);
			$user = $db->fetchAll('SELECT * FROM user where id = ? ', array($result[0]['user_id']), 2);
			if(!empty( $vendors && $user ) ){	
				foreach($vendors as $key => $value ){
					if(!empty($value['email'])){
						$this->sendEmail( $value['email'], $result[0]['title'], $user[0]['email'], 'vendorjobs.phtml', 'Quinceanera new job posted' );
					}
					$data = array( 
							'type'		=>	$params['type'],
							'title'		=>	$result[0]['title'],
							'notification_content'	=>	$result[0]['description'],
							//'status'	=>	
							'referer'	=>	$user[0]['email'],
							'referred'	=>	$value['email'],
							'added_on'	=>	date("Y-m-d H:i:s"),
							'updated_on'	=>	date("Y-m-d H:i:s")
					 );
					if( $data ) {
					    $db->insert("notifications", $data);
					}else{
						echo"not inserted";	
					}
				}
			}
		}
	}

	//@ Deepak function for apply job by vendor
	public function email_to_all_related_vendors_apply($job_id)
	{	
		$db=Zend_Registry::get("db");
		$SessDefault = new Zend_Session_Namespace('default');
		$SessVendor = new Zend_Session_Namespace('VendorSession');
		$SessUser = new Zend_Session_Namespace('UserSession');

		$job = 'SELECT * from job where id = ?';
		$result = $db->fetchAll($job, array($job_id), 2);
		if($result){
			$vendor = $db->fetchAll('SELECT * FROM appliers left join vendor on appliers.applier_id = vendor.user_id where appliers.job_id = ?', array($job_id), 2);   
			$users = $db->fetchAll('SELECT * from user where id = ? ', array($result[0]['user_id']), 2); 
			if(!empty( $vendor && $users )){ 
				foreach($vendor as $key => $value){
					if(!empty( $value['email'])){
						$this->sendEmail($users[0]['email'], $result[0]['title'],$value['email'], 'applyjob.phtml', 'Quinceanera applied job');
					}   

					$data = array( 
						'type'		=>	$params['type'],
						'title'		=>	$result[0]['title'],
						'notification_content'	=>	$result[0]['description'],
						//'status'	=>	
						'referer'	=>	$value['email'],
						'referred'	=>	$users[0]['email'],
						'added_on'	=>	date("Y-m-d H:i:s"),
						'updated_on'	=>	date("Y-m-d H:i:s")
					);
					if( $data ) {
					    $db->insert("notifications", $data);
					}else{
						echo"not inserted";	
					}
				}
			}
		}
	}



	//@ Deepak function for updating news by admin
	public function email_to_all_users()
	{
		
	}



	//@ Deepak function for messages between referer and referred
	public function email_to_referer_referred($mess_id)
	{	
		$db=Zend_Registry::get("db");
		$SessDefault = new Zend_Session_Namespace('default');
		$SessVendor = new Zend_Session_Namespace('VendorSession');
		$SessUser = new Zend_Session_Namespace('UserSession');
		
		$message = $db->fetchAll('SELECT * FROM messages where id = ? ', array($mess_id), 2);
		//echo "<pre>"; print_r($message); die;
		if($message){
			$referer = $db->fetchAll('SELECT * from user WHERE id = ? ', array($message[0]['sent_to']), 2);  
			$referred = $db->fetchAll('SELECT * from user WHERE id = ? ', array($message[0]['by_from']), 2);
			if( !empty( $referer && $referred ) ){
				$this->sendEmail($referred[0]['email'], NULL, $referer[0]['email'], 'message.phtml', 'You have recived one new message');
			}else{
				echo"Not send";
			}

			$data = array( 
					//'type'		=>	$image[0]['type'],
					'notification_content'	=>	$message[0]['message'],
					//'status'	=>	
					'referer'	=>	$referer[0]['email'],
					'referred'	=>	$referred[0]['email'],
					'added_on'	=>	date("Y-m-d H:i:s"),
					'updated_on'	=>	date("Y-m-d H:i:s")
				 );
				if( $data ) {
				    $db->insert("notifications", $data);
				}else{
					echo"not inserted";	
				}
			
		}
	}

	//@ Deepak function for uploading image by vendor
	public function email_to_all_related_users_image($id, $section)
	{
		$db=Zend_Registry::get("db");
		$SessDefault = new Zend_Session_Namespace('default');
		$SessVendor = new Zend_Session_Namespace('VendorSession');
		$SessUser = new Zend_Session_Namespace('UserSession');
	
		$image = $db->fetchAll('SELECT * FROM media WHERE id = ? and section = ?', array($id, $section), 2);
		if($image){
			$user = $db->fetchAll('SELECT * from user left join favorites on favorites.user_id=user.id  where favorite_id=?', array($image[0]['owner']), 2);
			$vendor = $db->fetchAll("SELECT email from user where role = '2'", array($image[0]['owner']), 2);				
			if(!empty($vendor)){			
				foreach($vendor as $key => $values){
					if(!empty($user)){
						foreach($user as $key => $value){
							if(!empty( $value['email'])){
								$this->sendEmail($value['email'],NULL, $values['email'], 'image.phtml', 'Quinceanera added new image');
							}else{ 
								echo"not send";
							 }
								
							$data = array( 
								'type'		=>	$image[0]['type'],
								'notification_content'	=>	$image[0]['description'],
								//'status'	=>	
								'referer'	=>	$values['email'],
								'referred'	=>	$value['email'],
								'added_on'	=>	date("Y-m-d H:i:s"),
								'updated_on'	=>	date("Y-m-d H:i:s")
							 );
							if( $data ) {
							    $db->insert("notifications", $data);
							}else{
								echo"not inserted";	
							}					
						}
					}
				}
			}
		}
	}

	//@ Deepak function for uploading video by vendor
	public function email_to_all_related_users_video($id, $section)
	{
		$db=Zend_Registry::get("db");
		$SessDefault = new Zend_Session_Namespace('default');
		$SessVendor = new Zend_Session_Namespace('VendorSession');
		$SessUser = new Zend_Session_Namespace('UserSession');
	
		$video = $db->fetchAll('SELECT * FROM media WHERE id = ? and section = ?', array($id, $section), 2);
		//echo print_r($video);die;
		if($video){
			$user = $db->fetchAll('SELECT * from user left join favorites on favorites.user_id=user.id  where favorite_id=?', array($video[0]['owner']), 2);
			$vendor = $db->fetchAll("SELECT email from user where role = '2'", array($video[0]['owner']), 2);	
			if(!empty($vendor)){			
				foreach($vendor as $key => $values){
					if(!empty($user)){
						foreach($user as $key => $value){
							if(!empty( $value['email'])){
								$this->sendEmail($value['email'],NULL, $values['email'], 'video.phtml', 'Quinceanera added new video');
							}else{
								echo "not send"; 
							}

							$data = array( 
								'type'		=>	$video[0]['type'],
								'notification_content'	=>	$video[0]['video_description'],
								//'status'	=>	
								'referer'	=>	$values['email'],
								'referred'	=>	$value['email'],
								'added_on'	=>	date("Y-m-d H:i:s"),
								'updated_on'	=>	date("Y-m-d H:i:s")
							 );
							if( $data ) {
							    $db->insert("notifications", $data);
							}else{
								echo"not inserted";	
							}
						}
					}
				}
			}
		}
	}

	// function for sending emails
	public function sendEmail($toemail, $title=NULL, $fromemail, $template, $subject){
		// create view object
		$SessUser = new Zend_Session_Namespace('UserSession');
		Zend_Loader::loadClass('Zend_View');
		$html = new Zend_View();
		$html->setScriptPath(APPLICATION_PATH . '/modules/pages/views/scripts/email/');
		
		// assign values

		$html->assign('user', $fromemail);
		$html->assign('email', $toemail);
		$html->assign('title', $title);
				
		// create mail object

		$mail = new Zend_Mail('utf-8');

		// render view
		$bodyText = $html->render($template);

		// configure base stuff
		$mail->addTo($toemail, '');
		$mail->setSubject($subject);
		$mail->setFrom($fromemail, 'No-reply');
		$mail->setBodyHtml($bodyText);		      
		$mail->send();  
	}
}

?>
