<?php

class Zend_View_Helper_FooterHelper extends Zend_View_Helper_Abstract
{
    function footer($bottomlinks)
    {
        $footer = '';
        $footerLinks = '';
        if ($bottomlinks) {

            foreach ($bottomlinks as $link) {

                $footerLinks .= <<<HTML
<a href="{$this->baseUrl()}/pages/index/view/type/{$link['post_type']}/id/{$link['ID']}"> {$link['post_title']}</a>
HTML;

            }
        }

        $footer .= <<<HTML
<div class="footer"><div class="wrapper">
		<div class="footer_top clear">
			<div class="col4">
				<h1 class="foot_title">Information</h1>
				<ul class="foot_links">
					<li><a href="#">Specials</a></li>
					<li><a href="#">Legal Notice</a></li>
					<li><a href="#">About Us</a></li>
					<li><a href="#">Our Stores</a></li>
					<li><a href="#">Contact Us</a></li>
					<li><a href="#">Sitemap</a></li>
				</ul>
			</div>
			<div class="col4">
				<h1 class="foot_title">Categories</h1>
				<ul class="foot_links">
					<li><a href="#">Men's</a></li>
					<li><a href="#">Women's</a></li>
					<li><a href="#">Dresses</a></li>
					<li><a href="#">Outerwear</a></li>
					<li><a href="#">Shoes</a></li>
					<li><a href="#">Jewelry</a></li>
					<li><a href="#">Accessories</a></li>
				</ul>
			</div>
			<div class="col4">
				<h1 class="foot_title">Contact Us</h1>
				<ul class="foot_links">
					<li>My Company</li>
					<li>42 avenue des Champs</li>
					<li>Elysées 75000 Paris France</li>
					<li>Tel 0123-456-789</li>
					<li><a href="mailto:">sales@yourcompany.com</a></li>
				</ul>
			</div>
			<div class="col4">
				<h1 class="foot_title">Further Info</h1>
				<ul class="foot_links">
					<li><a href="#">Terms and Conditions</a></li>
					<li><a href="#">Privacy Policy</a></li>
					<li><a href="#">Copyright</a></li>
				</ul>
			</div>
		</div>
		<div class="footer_bottom">
			<p>
				<a href="/">Home</a>
				<a href="/vendor/index">Advertise With Us</a>
				{$footerLinks}
			</p>
			<p>&copy; Copyright 15plug.com. All rights reserved.</p>
		</div>
	</div>
</div>
HTML;

        return $footer;
    }
}
