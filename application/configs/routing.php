<?php
$route = new Zend_Controller_Router_Route(
                                          'signup',
                                            array(
                                                'controller' => 'user',
                                                'action' => 'signup'
                                            )
                                        );

$router->addRoute('signup', $route);


$route = new Zend_Controller_Router_Route(
                                          'signin',
                                            array(
                                                'controller' => 'user',
                                                'action' => 'signin'
                                            )
                                        );

$router->addRoute('signin', $route);

$route = new Zend_Controller_Router_Route(
                                          'account_activation', 
                                            array(
                                                'controller' => 'user',
                                                'action' => 'account-activation'
                                            )
                                        );
$router->addRoute('account_activation', $route);


$route = new Zend_Controller_Router_Route(
                                          'forget_password', 
                                            array(
                                                'controller' => 'user',
                                                'action' => 'forget-password'
                                            )
                                        );

$router->addRoute('forget_password', $route);

$route = new Zend_Controller_Router_Route(
                                          'password_activation',
                                            array(
                                                'controller' => 'user',
                                                'action' => 'password-activation'
                                            )
                                        );

$router->addRoute('password_activation', $route);

/*
  route for vendor registraion.
*/
$route = new Zend_Controller_Router_Route(
                                          'vendor-registration', 
                                            array(
                                                'controller' => 'vendor',
                                                'action' => 'vendor-registration'
                                            )
                                        );
$router->addRoute('vendor-registration', $route);


/*
  route for logout user.
*/

    $route = new Zend_Controller_Router_Route(
                                              'logout', 
                                                array(
                                                    'controller' => 'user',
                                                    'action' => 'logout'
                                                )
                                            );
    $router->addRoute('logout', $route);

$route = new Zend_Controller_Router_Route(
                                          'user_profile',
                                            array(
                                                'controller' => 'user',
                                                'action' => 'user-profile'
                                            )
                                        );

$router->addRoute('user_profile', $route);



$route = new Zend_Controller_Router_Route(
                                          'payment_success',
                                            array(
                                                'controller' => 'vendor',
                                                'action' => 'payment-success'
                                            )
                                        );

$router->addRoute('payment_success', $route);

$route = new Zend_Controller_Router_Route(
                                          'subscription_success',
                                            array(
                                                'controller' => 'vendor',
                                                'action' => 'subscription-success'
                                            )
                                        );

$router->addRoute('subscription_success', $route);

