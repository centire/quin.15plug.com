<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
</div>
<div class="mode_footer">
<div class="mode_footer_main">
<div class="mode_copyright">
<div class="container_24">
	<div id="footer_copyright">
	<?php wp_nav_menu( array( 'theme_location' => 'secondary', 'menu_class' => 'footer_menu' ) ); ?>
	
	<?php get_sidebar( 'footer' ); ?>
		<?php  do_action( 'twentyfourteen_credits' ); ?><p class="copy_right">
			<a href="<?php echo esc_url( __( 'http://mastersoftwaretechnologies.com/za/public/', 'twentyfourteen' ) ); ?>">
				<?php printf( __( '2013 WeddingDays Prestashop Store. ', 'twentyfourteen' ), ' All rights reserved' ); ?>
			</a></p>
	</div>
</div>
</div>
</div>
</div>
	<?php wp_footer(); ?>