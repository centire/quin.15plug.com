<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header();
?>
<div class="cs_mode_contain">
<div class="mode_container">
<div class="container_24">
<div class="two_column grid_24 omega alpha" id="columns">
<div id="left_column" class="grid_4  alpha fadeInLeft animated" data-animate="fadeInLeft" data-delay="0">
<?php
	get_sidebar();
?>
</div>
<div id="center_column" class="grid_20 omega fadeInRight animated" data-animate="fadeInRight" data-delay="0">
<h1 class="h_blog">

				<?php _e( 'Not Found', 'twentyfourteen' ); ?>
			
				<?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentyfourteen' ); ?></p>

				<?php get_search_form(); ?>
</h1>
</div>
</div>
</div>
</div>
</div>

<?php
get_sidebar( 'content' );
get_footer();
