<?php
get_header("one"); ?>

    <div class="wrapper">
        <div class="left_content">
            <?php while ( have_posts() ) : the_post(); ?>

                <div class="single_post">
                    <h1 class="post_title"><?php the_title(); ?></h1>
                    <div class="meta_info">
                        <p>Posted by: <a href="#"><?php the_author();?></a> on <?php echo date('dS F Y', strtotime(get_the_date())); ?> <a href="<?php echo get_comment_link(the_comment()); ?>" class="comments_toggle"><?php echo comments_number(); ?></a></p>
                    </div>
                    <div class="post_info">
                        <div class="post_img">
                            <?php the_post_thumbnail('', array('class' => 'align')); ?>
                        </div>
                        <div class="post_description">
                            <?php the_excerpt(); ?>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <?php endwhile; ?>
        </div>
        <div class="rightbar_content">
            <?php get_sidebar('one'); ?>
        </div>
        <div class="clear"></div>
    </div>
<?php
get_footer("one");
