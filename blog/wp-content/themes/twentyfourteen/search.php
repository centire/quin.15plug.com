<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div class="cs_mode_contain">
<div class="mode_container">
<div class="container_24">
<div class="two_column grid_24 omega alpha" id="columns">
<div id="left_column" class="grid_4  alpha fadeInLeft animated" data-animate="fadeInLeft" data-delay="0">
<?php
	get_sidebar();
?>
</div>
<div id="center_column" class="grid_20 omega fadeInRight animated" data-animate="fadeInRight" data-delay="0">

			<?php if ( have_posts() ) : ?>
<h1 class="h_blog">

			<?php printf( __( 'Search Results for: %s', 'twentyfourteen' ), get_search_query() ); ?>
</h1>

				<?php
					// Start the Loop.
					while ( have_posts() ) : the_post();

						/*
						 * Include the post format-specific template for the content. If you want to
						 * use this in a child theme, then include a file called called content-___.php
						 * (where ___ is the post format) and that will be used instead.
						 */
						get_template_part( 'content', get_post_format() );

					endwhile;
					// Previous/next post navigation.
					twentyfourteen_paging_nav();

				else :
					// If no content, include the "No posts found" template.
					get_template_part( 'content', 'none' );

				endif;
			?>
</div>
</div>
</div>
</div>
</div>

<?php
get_sidebar( 'content' );
get_footer();
