<?php
/**
 * The template for displaying Category pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header("one");
?>






    <div class="wrapper">
        <div class="left_content">
            <!--<h1 class="archive-title"><?php printf( __( 'Category Archives: %s', 'twentyfourteen' ),single_cat_title( '', false ) ); ?></h1>-->
            <br>
            <?php
            // Show an optional term description.
            $term_description = term_description();
            if ( ! empty( $term_description ) ) :
                printf( '<div class="taxonomy-description">%s</div>', $term_description );
            endif;
            ?>
            <?php while ( have_posts() ) : the_post(); ?>

                <div class="single_post">
                    <h1 class="post_title"><?php single_cat_title(''); ?></h1>
                    <div class="meta_info">
                        <p>Posted by: <a href="#"><?php the_author();?></a> on <?php echo date('dS F Y', strtotime(get_the_date())); ?> <a href="<?php echo get_comment_link(the_comment()); ?>" class="comments_toggle"><?php echo comments_number(); ?></a></p>
                    </div>
                    <div class="post_info">
                        <div class="post_img">
                            <?php the_post_thumbnail('', array('class' => 'align')); ?>
                        </div>
                        <div class="post_description">
                            <p><?php the_excerpt(); ?></p>
                            <a href="<?php echo get_permalink(); ?>" class="read_more_btn">Read More</a>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            <?php endwhile;
            twentyfourteen_paging_nav(); ?>
        </div>
        <div class="rightbar_content">
            <div class="sub_block">
                <h1 class="sub_title">Subscribe to our blog</h1>
                <form>
                    <div class="form_control">
                        <input type="email" placeholder="Enter your email" />
                    </div>
                    <div class="form_control">
                        <input type="password" placeholder="Enter your password" />
                    </div>
                    <div class="form_control">
                        <input type="submit" value="Subscribe" />
                    </div>
                </form>
            </div>

            <!-- Category -->
            <div class="sub_block">
                <h1 class="sub_title">Categories</h1>
                <?php get_sidebar('one');?>
            </div>
        </div>
        <div class="clear"></div>
    </div>
<?php
get_footer("one");
