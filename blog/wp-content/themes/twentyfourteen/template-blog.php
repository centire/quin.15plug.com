<?php
/**
 * Template Name: Blog
 */

// Get header
get_header("one");
?>

<div class="wrapper">
    <div class="left_content">
        <?php
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array('posts_per_page' => 3, 'paged' => $paged);
        $count = 0;

        // If posts have
        if ( have_posts() ) :

    query_posts( $args );

    // The Loop
    while ( have_posts() ) : the_post(); ?>

        <!-- Single post -->
        <div class="single_post">
            <h1 class="post_title"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h1>
            <div class="meta_info"><p>Posted by: <a href="#"><?php the_author();?></a> on
                    <?php echo date('dS F Y', strtotime(get_the_date())); ?>
                    <a href="<?php echo get_comment_link() ?>" class="comments_toggle"><?php echo comments_number();?></a></p></div>
            <div class="post_info">
                <div class="post_img">
                    <a href="<?php echo get_permalink(); ?>"><?php the_post_thumbnail( ); ?></a>
                </div>
                <div class="post_description">
                    <p><?php the_excerpt(); ?></p>
                    <a href="<?php echo get_permalink(); ?>" class="read_more_btn">Read More</a>
                </div>
                <div class="clear"></div>
            </div>
        </div>

    <?php
    $count++;
    endwhile;
    ?>
    <!-- End of the main loop -->

    </div>

    <div class="rightbar_content">
        <?php get_sidebar('one'); ?>
    </div>
    <div class="clear"></div>

        <ul class="pagination">
            <li class="active">
                <?php
                if ( $wp_query->max_num_pages > 1 ) : ?>
                <?php for ( $i = 1; $i <= $wp_query->max_num_pages; $i ++ ) {
                $link = $i == 1 ? remove_query_arg( 'paged' ) : add_query_arg( 'paged', $i );
                ?>&nbsp; &nbsp;
            <li>
                <?php echo '<a href="' . $link . '"' . ( $i == $paged ? '' : '' ) . '>' .$i . '</a>'; ?>
            </li>
            <?php  } ?>
            </li>
        </ul>
    <?php endif ?>
    <?php else :
        // If no content, include the "No posts found" template.
        echo "No posts found";
    endif; ?>


</div>

<!--Footer-->
<?php get_footer("one"); ?>




