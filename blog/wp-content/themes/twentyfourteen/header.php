<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>s
	<![endif]-->
	<?php wp_head(); ?>
</head>
<body <?php  body_class(); ?>>
<div id="page" class="hfeed site">
	<div id="page">
	<div class="mode_header">
        <div class="mode_header_content">
        <div class="container_24">
	<div id="header" class="grid_24 clearfix omega alpha">
        <div id="header_left" class="grid_8 alpha">
		<ul id="header_links">
			<?php wp_nav_menu( array( 'theme_location' => 'upper header', 'menu_class' => 'nav-menu2' ) ); ?>
		
		</ul>
<?php if ( is_active_sidebar( 'sidebar-5' ) ) : ?>
	<?php dynamic_sidebar( 'sidebar-5' ); ?>
<?php endif; ?>
                </div>
                	<div id="header_content" class="grid_8">	
<?php if ( get_header_image() ) : ?>
		<a title="<?php bloginfo('description'); ?> " id="header_logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
				<img class="logo" alt="<?php bloginfo('description'); ?> " src="<?php header_image(); ?>">
		</a>
<?php endif; ?>	
		
</div>
<div id="header_right" class="grid_8 omega">
	
</div>
</div>
</div>
</div>
</div>
<div class="cs_both_mode">
<div class="mode_megamenu">
<div class="container_24">
	
<div class="bc_line">
	<?php if(function_exists(simple_breadcrumb)) {simple_breadcrumb();} ?>
</div>
</div>
</div>
</div>
</div>
<!--<div class="search-toggle">-->
<!--	<a href="#search-container" class="screen-reader-text"><?php _e( 'Search', 'twentyfourteen' ); ?></a>-->
<!--</div>-->

<!--<nav id="primary-navigation" class="site-navigation primary-navigation" role="navigation">-->
<!--	<!--<button class="menu-toggle"><?php _e( 'Primary Menu', 'twentyfourteen' ); ?></button>-->
<!--	<a class="screen-reader-text skip-link" href="#content"><?php _e( 'Skip to content', 'twentyfourteen' ); ?></a>-->
<!--	<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>-->
<!--</nav>-->

<!--
<div id="search-container" class="search-box-wrapper hide">
<div class="search-box">
<?php //get_search_form(); ?>
</div>
</div>
-->
<!-- #masthead -->


