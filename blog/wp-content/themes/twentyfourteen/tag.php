<?php
/**
 * The template for displaying Tag pages
 *
 * Used to display archive-type pages for posts in a tag.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<div class="cs_mode_contain">
<div class="mode_container">
<div class="container_24">
<div class="two_column grid_24 omega alpha" id="columns">
<div id="left_column" class="grid_4  alpha fadeInLeft animated" data-animate="fadeInLeft" data-delay="0">
<?php
get_sidebar();
?>
</div>
<div id="center_column" class="grid_20 omega fadeInRight animated" data-delay="0" data-animate="fadeInRight">

<h3 class="nbresult">
		<span class="big">
			<?php echo count(the_post); ?> results have been found.
		</span>
</h3>

<ul id="product_list" class="product_grid">
				<?php if ( have_posts() ) : ?>

		<?php
					// Start the Loop.
					while ( have_posts() ) : the_post(); ?>
	<li class="ajax_block_product clearfix omega alpha">
	<div class="post_name">
		<?php
						/*
						 * Include the post format-specific template for the content. If you want to
						 * use this in a child theme, then include a file called called content-___.php
						 * (where ___ is the post format) and that will be used instead.
						 */
		?>
	<h3>
			<a title="<?php the_title(); ?>" href="<?php the_permalink() ?>"><?php the_title(); ?></a>
	</h3>
	</div>
	<div class="post_image">
		<a title="<?php the_title(); ?>" href="<?php the_permalink() ?>">
			<?php the_post_thumbnail('', array('class' => 'alignright')); ?>
		</a>
	</div>
	<div class="post_description">
		<p style="margin:0 -151px;"><?php the_content(); ?></p>
	</div>
	<div class="author">
		<p class="comment">
	<?php
		comments_number('0', '1', '%');
	?>
		<span> Comment </span></p><br>
		<p>Posted by <span>
	<?php
		the_author();
	?>
	</span></p></div></li>
		<?php

					endwhile;

				else :
					// If no content, include the "No posts found" template.
					get_template_part( 'content', 'none' );

				endif;
			?>

	</ul></div></div></div></div>		
<?php
get_footer();

