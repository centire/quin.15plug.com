<div class="wrap" id="wgpag-options">
	<div class="icon32" id="icon-themes"></div>

	<h2>Widget Paginator <?php _e("Settings") ?></h2>

	<p>
		<?
		printf(__("Before you can see the widgets %s, %s, %s, %s, %s, %s or %s as a paginated list, you need to add them to your %s.", 'wgpag'),
					'<a href="http://en.support.wordpress.com/widgets/archives-widget/">'.__("Archives").'</a>',
					'<a href="http://en.support.wordpress.com/widgets/categories-widget/">'. __("Categories").'</a>',
					'<a href="http://en.support.wordpress.com/widgets/links-widget/">'. __("Links").'</a>',
					'<a href="http://en.support.wordpress.com/widgets/meta-widget/">'.__("Meta").'</a>',
					'<a href="http://en.support.wordpress.com/widgets/pages-widget/">'. __("Pages").'</a>',
					'<a href="http://en.support.wordpress.com/widgets/recent-comments-widget/">'. __("Recent Comments").'</a>',
					'<a href="http://en.support.wordpress.com/widgets/recent-posts-widget/">'. __("Recent Posts").'</a>',
					'<a href="widgets.php">'. __("Widget Areas") .'</a>');
		?>
	</p>

	<?php
	$default = __("Default");
	$empty   = __("empty", 'wgpag');

	if ( isset($_POST['wgpag']) && $_POST['wgpag']=='true' ) {

		//TODO: check, if this saving was successfull

		echo '<div id="message" class="updated" style="margin:30px auto 20px; width:600px; cursor:pointer;" onclick="jQuery(\'div#message\').css(\'display\',\'none\');">
				<p style="float:right; font-size:10px; font-variant:small-caps; color:#600000; padding-top:4px;">(close)</p>
				<p><strong>Your settings have been saved.</strong></p>
			</div>';
	}

	// print '<pre>' . print_r ($options, true) . '</pre>';
	?>

	<form method="post" action="#">
		<div class="metabox-holder" style="width:30%; float:left; margin-right:3%;">
			<div class="postbox">
				<h3><?php _e("Items per Page", 'wgpag') ?>*</h3>

				<p><?php _e("If you want a pagination, enter the designated items per page for each widget. Otherwise, leave it emtpy.", 'wgpag') ?></p>

				<table class="form-table">
					<?php
					foreach ($this->widgets as $w => $name) :
						$value = $options['items_per_page_' . $w];
						$name = __($name);
						?>
						<tr>
							<th>
								<label for="wgpag-items_per_page_<?php echo $w ?>"><?php echo $name ?>:</label>
							</th>
							<td>
								<input type="text" maxlength="2" size="2"
									name="wgpag-items_per_page_<?php echo $w ?>"
									id="wgpag-items_per_page_<?php echo $w ?>"
									value="<?php echo $value ?>" />
								<span class="hint"><?php echo $default ?>: <?php echo $empty ?></span>
							</td>
						</tr>
					<?php endforeach; ?>
				</table>
			</div><!-- /postbox -->

			<div class="postbox">
				<h3><?php _e("Pagination Options", 'wgpag') ?></h3>

				<p><?php _e("Here you can change the defaults for how the pagination is returned.", 'wgpag') ?></p>

				<table class="form-table">
					<tr>
						<th>
							<label for="wgpag-pag_option_ptsh"><?php _e("Max. pagination items", 'wgpag') ?>:*</label>
						</th>
						<td>
							<input type="text" maxlength="2" size="5"
								name="wgpag-pag_option_ptsh"
								id="wgpag-pag_option_ptsh"
								value="<? echo $options['pag_option_ptsh'] ? $options['pag_option_ptsh'] : 7; ?>" />
							<span class="hint"> <? echo $default ?>: 7</span>
						</td>
					</tr>

					<tr>
						<th>
							<label for="wgpag-pag_option_prev"><?php _e("Previous label", 'wgpag') ?>:</label>
						</th>
						<td>
							<input type="text" size="5"
								name="wgpag-pag_option_prev"
								id="wgpag-pag_option_prev"
								value="<? echo $options['pag_option_prev'] ? $options['pag_option_prev'] : '<'; ?>" />
							<span class="hint"><?php _e("e.g.", 'wgpag') ?> &#9664; <?php _e("or", 'wgpag') ?> &larr; &nbsp;|&nbsp; <? echo $default ?>: &lt;</span>
						</td>
					</tr>

					<tr>
						<th>
							<label for="wgpag-pag_option_next"><?php _e("Next label", 'wgpag') ?>:</label>
						</th>
						<td>
							<input type="text" size="5"
								name="wgpag-pag_option_next"
								id="wgpag-pag_option_next"
								value="<? echo $options['pag_option_next'] ? $options['pag_option_next'] : '>'; ?>" />
							<span class="hint"><?php _e("e.g.", 'wgpag') ?> &#9654; <?php _e("or", 'wgpag') ?> &rarr; &nbsp;|&nbsp; <? echo $default ?>: &gt;</span>
						</td>
					</tr>

					<tr>
						<th>
							<label for="wgpag-pag_option_prevnext_threshold"><?php _e("Visibility of the labels", 'wgpag') ?>:*</label>
						</th>
						<td>
							<input type="text" size="5" maxlength="5"
								name="wgpag-pag_option_prevnext_threshold"
								id="wgpag-pag_option_prevnext_threshold"
								value="<? echo $options['pag_option_prevnext_threshold'] ?>" />
							<span class="hint"><?php _e("e.g.", 'wgpag') ?> 3 | <? echo $default ?>:  (<?php _e("off", 'wgpag') ?>)</span>
						</td>
					</tr>

					<tr>
						<th>
							<label><?php _e("Horizontal alignment", 'wgpag') ?>:</label>
						</th>
						<td>
							<?
							$wgpag_pag_option_hor_align = $options['pag_option_hor_align'];
							?>
							<label>
								<input type="radio"
									name="wgpag-pag_option_hor_align"
									id="wgpag-pag_option_hor_align_left"
									value="left"
									<? if ($wgpag_pag_option_hor_align == 'left')
										echo 'checked="checked"'; ?> /><?php _e("left", 'wgpag') ?>
							</label> &nbsp;
							<label>
								<input type="radio"
									name="wgpag-pag_option_hor_align"
									id="wgpag-pag_option_hor_align_center"
									value="center"
									<? if ($wgpag_pag_option_hor_align == 'center' || !$wgpag_pag_option_hor_align)
										echo 'checked="checked"'; ?> /><?php _e("center", 'wgpag') ?>
							</label> &nbsp;
							<label>
								<input type="radio"
									name="wgpag-pag_option_hor_align"
									id="wgpag-pag_option_hor_align_right"
									value="right"
									<? if ($wgpag_pag_option_hor_align == 'right')
										echo 'checked="checked"'; ?> /><?php _e("right", 'wgpag') ?>
							</label> &nbsp;
							<span class="hint"><? echo $default ?>: <?php _e("center", 'wgpag') ?></span>
						</td>
					</tr>

					<tr>
						<th>
							<label for="wgpag-pag_option_margin_top"><?php _e("Top margin", 'wgpag') ?>:</label>
						</th>
						<td>
							<input type="text" size="5" maxlength="5"
								name="wgpag-pag_option_margin_top"
								id="wgpag-pag_option_margin_top"
								value="<? echo $options['pag_option_margin_top'] ?>" />
							<span class="hint"><?php _e("e.g.", 'wgpag') ?> 10px <?php _e("or", 'wgpag') ?> 1.0em &nbsp;|&nbsp; <? echo $default ?>: 0.5em</span>
						</td>
					</tr>
					<tr>
						<th>
							<label for="wgpag-pag_option_margin_bottom"><?php _e("Bottom margin", 'wgpag') ?>:</label>
						</th>
						<td>
							<input type="text" size="5" maxlength="5"
								name="wgpag-pag_option_margin_bottom"
								id="wgpag-pag_option_margin_bottom"
								value="<? echo $options['pag_option_margin_bottom'] ?>" />
							<span class="hint"><? echo $default ?>: <?php _e("empty", 'wgpag') ?></span>
						</td>
					</tr>
					<tr>
						<th>
							<label for="wgpag-pag_option_autoscroll"><?php _e("Auto-scroll speed", 'wgpag') ?>:</label>
						</th>
						<td>
							<input type="text" size="5" maxlength="5"
								name="wgpag-pag_option_autoscroll_speed"
								id="wgpag-pag_option_autoscroll_speed"
								value="<? echo $options['pag_option_autoscroll_speed'] ?>" />
							<span class="hint"><? echo $default ?>: 0 (<?php _e("off", 'wgpag') ?>) in ms</span>
						</td>
					</tr>
				</table>
			</div><!-- /postbox -->

			<p class="submit">
				<input type="hidden"
					   value="<?php echo wp_create_nonce('wgpag-nonce'); ?>"
					   name="wgpag-nonce" />
				<input type="hidden" value="true" name="wgpag" />
				<input type="submit" value="<?php _e('Save Changes') ?>"
					   class="button-primary" id="submit" name="submit" />
			</p>

		</div><!-- /metabox-holder -->

		<div class="metabox-holder" style="width:30%; float:left; margin-right:3%;">

			<div class="postbox">
				<h3><?php _e("Styling Options", 'wgpag') ?></h3>

				<p><?php _e("If you want to change the appearance of the pagination, enter the designated value. Otherwise, leave it emtpy.", 'wgpag') ?></p>

				<h4><?php _e("Current pagination item", 'wgpag') ?>*</h4>

				<table class="form-table">
					<tr>
						<th>
							<label for="wgpag-cur_item_style_color"><?php _e("Text colour", 'wgpag') ?>:</label>
						</th>
						<td>
							<input type="text" size="7" maxlength="7"
								name="wgpag-cur_item_style_color"
								id="wgpag-cur_item_style_color"
								class="color-picker"
								value="<? echo $options['cur_item_style_color'] ?>" />
							<span class="hint"><? echo $default ?>: <?php _e("from theme", 'wgpag') ?></span>
						</td>
					</tr>
					<tr>
						<th>
							<label for="wgpag-cur_item_style_border-color"><?php _e("Border colour", 'wgpag') ?>:</label>
						</th>
						<td>
							<input type="text" size="7" maxlength="7"
								name="wgpag-cur_item_style_border-color"
								id="wgpag-cur_item_style_border-color"
								class="color-picker"
								value="<? echo $options['cur_item_style_border-color'] ?>" />
							<span class="hint"><? echo $default ?>: <?php _e("transparent", 'wgpag') ?></span>
						</td>
					</tr>
					<tr>
						<th>
							<label for="wgpag-cur_item_style_background-color"><?php _e("Background colour", 'wgpag') ?>:</label>
						</th>
						<td>
							<input type="text" size="7" maxlength="7"
								name="wgpag-cur_item_style_background-color"
								id="wgpag-cur_item_style_background-color"
								class="color-picker"
								value="<? echo $options['cur_item_style_background-color'] ?>" />
							<span class="hint"><? echo $default ?>: #F1F1F1</span>
						</td>
					</tr>
					<tr>
						<th>
							<label for="wgpag-cur_item_style_font-size"><?php _e("Font size", 'wgpag') ?>:</label>
						</th>
						<td>
							<input type="text" size="7" maxlength="5"
								name="wgpag-cur_item_style_font-size"
								id="wgpag-cur_item_style_font-size"
								value="<? echo $options['cur_item_style_font-size'] ?>" />
							<span class="hint"><?php _e("e.g.", 'wgpag') ?> 12px <?php _e("or", 'wgpag') ?> 0.9em &nbsp;|&nbsp; <? echo $default ?>: <?php _e("from theme", 'wgpag') ?></span>
						</td>
					</tr>
				</table>

				<h4><?php _e("Linked pagination items", 'wgpag') ?>*</h4>

				<table class="form-table">
					<tr>
						<th>
							<label for="wgpag-item_style_color"><?php _e("Text colour", 'wgpag') ?>:</label>
						</th>
						<td>
							<input type="text" size="7" maxlength="7"
								name="wgpag-item_style_color"
								id="wgpag-item_style_color"
								class="color-picker"
								value="<? echo $options['item_style_color'] ?>" />
							<span class="hint"><? echo $default ?>: <?php _e("from theme", 'wgpag') ?></span>
						</td>
					</tr>
					<tr>
						<th>
							<label for="wgpag-item_style_border-color"><?php _e("Border colour", 'wgpag') ?>:</label>
						</th>
						<td>
							<input type="text" size="7" maxlength="7"
								name="wgpag-item_style_border-color"
								id="wgpag-item_style_border-color"
								class="color-picker"
								value="<? echo $options['item_style_border-color'] ?>" />
							<span class="hint"><? echo $default ?>: #F1F1F1</span>
						</td>
					</tr>
					<tr>
						<th>
							<label for="wgpag-item_style_background-color"><?php _e("Background colour", 'wgpag') ?>:</label>
						</th>
						<td>
							<input type="text" size="7" maxlength="7"
								name="wgpag-item_style_background-color"
								id="wgpag-item_style_background-color"
								class="color-picker"
								value="<? echo $options['item_style_background-color'] ?>" />
							<span class="hint"><? echo $default ?>: <?php _e("transparent", 'wgpag') ?></span>
						</td>
					</tr>
					<tr>
						<th>
							<label for="wgpag-item_style_font-size"><?php _e("Font size", 'wgpag') ?>:</label>
						</th>
						<td>
							<input type="text" size="7" maxlength="5"
								name="wgpag-item_style_font-size"
								id="wgpag-item_style_font-size"
								value="<? echo $options['item_style_font-size'] ?>" />
							<span class="hint"><? echo $default ?>: <?php _e("from theme", 'wgpag') ?></span>
						</td>
					</tr>
				</table>

				<h4><?php _e("Mouseover on linked pagination items", 'wgpag') ?>*</h4>

				<table class="form-table">
					<tr>
						<th>
							<label for="wgpag-hover_item_style_color"><?php _e("Text colour", 'wgpag') ?>:</label>
						</th>
						<td>
							<input type="text" size="7" maxlength="7"
								name="wgpag-hover_item_style_color"
								id="wgpag-hover_item_style_color"
								class="color-picker"
								value="<? echo $options['hover_item_style_color'] ?>" />
							<span class="hint"><? echo $default ?>: <?php _e("from theme", 'wgpag') ?></span>
						</td>
					</tr>
					<tr>
						<th>
							<label for="wgpag-hover_item_style_border-color"><?php _e("Border colour", 'wgpag') ?>:</label>
						</th>
						<td>
							<input type="text" size="7" maxlength="7"
								name="wgpag-hover_item_style_border-color"
								id="wgpag-hover_item_style_border-color"
								class="color-picker"
								value="<? echo $options['hover_item_style_border-color'] ?>" />
							<span class="hint"><? echo $default ?>: <?php _e("from theme", 'wgpag') ?></span>
						</td>
					</tr>
					<tr>
						<th>
							<label for="wgpag-hover_item_style_background-color"><?php _e("Background colour", 'wgpag') ?>:</label>
						</th>
						<td>
							<input type="text" size="7" maxlength="7"
								name="wgpag-hover_item_style_background-color"
								id="wgpag-hover_item_style_background-color"
								class="color-picker"
								value="<? echo $options['hover_item_style_background-color'] ?>" />
							<span class="hint"><? echo $default ?>: <?php _e("from theme", 'wgpag') ?></span>
						</td>
					</tr>
					<tr>
						<th>
							<label for="wgpag-hover_item_style_font-size"><?php _e("Font size", 'wgpag') ?>:</label>
						</th>
						<td>
							<input type="text" size="7" maxlength="5"
								name="wgpag-hover_item_style_font-size"
								id="wgpag-hover_item_style_font-size"
								value="<? echo $options['hover_item_style_font-size'] ?>" />
							<span class="hint"><? echo $default ?>: <?php _e("from theme", 'wgpag') ?></span>
						</td>
					</tr>
				</table>

				<h4><?php _e("List Items", 'wgpag') ?></h4>

				<table class="form-table">
					<tr>
						<th>
							<label for="wgpag-list_item_style"><?php _e("List style type", 'wgpag') ?>:</label>
						</th>
						<td>
							<select name="wgpag-list_item_style"
									id="wgpag-list_item_style">
								<option value="inherit"
									<?php if ($options['list_item_style'] == 'inherit')
										echo 'selected="selected"'; ?>>
									<?php _e("from theme", 'wgpag') ?>&nbsp;</option>
								<option value="none"
									<?php if ($options['list_item_style'] == 'none')
										echo 'selected="selected"'; ?>>
									<?php _e("none", 'wgpag') ?>&nbsp;</option>
								<option value="square"
									<?php if ($options['list_item_style'] == 'square')
										echo 'selected="selected"'; ?>>
									■ <?php _e("Square", 'wgpag') ?> </option>
								<option value="disc"
									<?php if ($options['list_item_style'] == 'disc')
										echo 'selected="selected"'; ?>>
									&#9679; <?php _e("Disc", 'wgpag') ?></option>
								<option value="circle"
									<?php if ($options['list_item_style'] == 'circle')
										echo 'selected="selected"'; ?>>
									○ <?php _e("Circle", 'wgpag') ?></option>
							</select>
							<span class="hint"><? echo $default ?>: <?php _e("from theme", 'wgpag') ?></span>
						</td>
					</tr>
				</table>
			</div><!-- /postbox -->
		</div><!-- /metabox-holder -->
	</form>

	<div class="metabox-holder" style="width:30%; float:right;">

		<div class="postbox">
			<h3><?php _e("Contact the Plugin Developers", 'wgpag') ?></h3>

			<p>
				<?php _e("You have a question, want to report a bug, help translating, or suggest a feature", 'wgpag') ?>?
			</p>
			<p>
				&rarr; <a href="http://wordpress.org/tags/widget-pagination?forum_id=10">Plugin Forum</a><br />
				&rarr; <a href="http://wgpag.jana-sieber.de/">Plugin Homepage</a>
			</p>
		</div>

		<div class="postbox">
			<h3><?php _e("Theme Compatibility", 'wgpag') ?></h3>

			<p>
				<?php _e("This plugin might not work in custom themes, if the lists are generated in a different way than in the standard wordpress themes. We are working on increasing the compatibility of this plugin. Feel free to drop us a link to your theme or your page.", 'wgpag') ?>
			</p>
			<p>
				<?php _e("Note for Developers", 'wgpag') ?>:
				<br />
				<?
				printf(__("If you are using %s, just set the parameter *class* to *widget_links*. Meanwhile, we are working on a solution for %s.", 'wgpag'),
						'<a href="http://codex.wordpress.org/Function_Reference/wp_list_bookmarks"><i>wp_list_bookmarks()</i></a>',
						'wp_get_archives(), wp_list_categories(), wp_list_authors(), wp_list_pages() and wp_list_comments()');
				?>
			</p>
		</div>

		<div class="postbox">
			<h3>* <?php _e("Legend", 'wgpag') ?></h3>

			<p>
				<img src="<?php echo plugin_dir_url( __FILE__ ) ?>../images/legend.png"
					alt="<?php _e("Legend", 'wgpag') ?>"
					title="<?php _e("screenshot of paginated links widget", 'wgpag') ?>"
					class="left"/>
				<?php _e("The image shows the 1st page of a paginated links widget, with 2 items per page and a max. number of 4 pages to show.", 'wgpag') ?>
			</p>

			<dl>
				<dt><?php _e("Items per Page", 'wgpag') ?> (a)</dt>
				<dd>
					<?php _e("This number distributes all widget links on x pages, so that you see this number of links on each of the pages. (e.g. 2)", 'wgpag') ?>
				</dd>

				<dt><?php _e("Max. pagination items", 'wgpag') ?> (b)</dt>
				<dd>
					<?php _e("Maximum number of pages shown in the pagination. If there are more pages, a separator (&hellip;) is shown.", 'wgpag') ?>
				</dd>

				<dt><?php _e("Current pagination item", 'wgpag') ?> (c)</dt>
				<dd>
					<?php _e("The currently open page number (hence not a link).", 'wgpag') ?>
				</dd>

				<dt><?php _e("Linked pagination items", 'wgpag') ?> (d)</dt>
				<dd>
					<?php _e("The page numbers and the previous/next links.", 'wgpag') ?>
				</dd>

				<dt><?php _e("Mouseover on linked pagination items", 'wgpag') ?> (e)</dt>
				<dd>
					<?php _e("An effect to highlight interactive elements.", 'wgpag') ?>
				</dd>

				<dt><?php _e("Visibility of the labels", 'wgpag') ?> (f)</dt>
				<dd>
					<?php _e("Only show the previous and next labels if there are more pages in the pagination than the number given in this setting.)", 'wgpag') ?>
				</dd>

				<dt><?php _e("px/em units", 'wgpag') ?></dt>
				<dd>
					<?php _e("If you want to set fixed font sizes or margins, use the absolute, theme-independent unit px. If you want them to depend on your theme though, use the relative unit em. (1em corresponds to 100% resp. 1 line)", 'wgpag') ?>
				</dd>
			</dl>

		</div>
	</div><!-- /metabox-holder -->
</div><!-- /wrap -->
<!-- required to clear for additional content -->
<div style="clear:both;"></div>
