=== Widget Paginator ===
Contributors: jasie, larsu
Donate link: http://wgpag.jana-sieber.de/donate/
Tags: pagination, paging, paginator, widget, categories, archives, blogroll, links, bookmarks, recent posts, recent comments, meta, pages
Requires at least: 2.9
Tested up to: 3.4
Stable tag: 0.8
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin lets you add a stylable pagination for the widgets Archives, Categories, Links, Meta, Pages, Recent Comments and Recent Posts.

== Description ==

Currently, you can chose the widget, which is to be paginated, and set the items to show per widget page individually. You can also set the number of pages to show and change the labels of the previous/next link, as well as let the widget pages turn automatically. Finally, you can change the default styling to match your theme.

**Plugin's Official Site**

See a demo at: http://wgpag.jana-sieber.de/

**Languages**

* English
* Deutsch
* Român (provided by Alexander Ovsov)

**Browser Compatibility**

positively tested with Firefox, Chrome, Opera and Internet Explorer 7-9

**Plans**

* animated paging (a few sexy effects)
* support of more widget types than the current standard 7 ones (what others do you need paginated?)
* instant preview of option changes on the plugin settings page (we don’t like surprises, do we)

== Installation ==

Use wordpress' built-in installer, or upload and unzip widget_pagination.zip to your /wp-content/plugins/ directory.

You will find 'Widget Paginator' menu in your WordPress admin panel in the 'Appearance' area (where you also add widgets to the sidebar).

== Screenshots ==

1. Examples for some widgets in the Twenty Ten and Twenty Eleven Wordpress Theme.

2. Example for plugin settings.

== Frequently Asked Questions ==

= Does the plugin work for other widget types, too? =

Sorry, currently it only works for the standard widgets, that come with Wordpress.

= How can I prevent my pagination from being broken up on two lines? =

You can try to decrease the 'max. items' count and/or the font sizes in the settings.

= How can I change the order of the list elements? =

Sorry, the plugin is not able to do that as the lists are generated by the theme.

= Why can't I see a pagination on my widgets? =

Either your settings are not correct (check the 2nd screenshot) or your theme generates the widgets differently than Wordpress. Send us a link to your website, then we will try to help.

== Changelog ==

**0.7.2**

* New option: set auto-scrolling (pages change automatically after a chosen time)
* New option: previous/next labels can be hidden
* New language: Român (thanks to Alexander Ovsov)
* Optimisation: option saving
* Minor bugfixes

**0.7.1**

* Bugfix: allow empty value / zero for margins
* New option: chose list bullet type
* New option: pagination for Pages Widget

**0.7**

* Bugfix: decimal em values can be entered now
* Bugfix: don't show separator if it only hides a single page number
* Improvement: avoid possible jumping of pagination on last page (works only for links taking up the same number of lines)
* New option: allows horizontal positioning of pagination

**0.6.3**

* Added legend to admin page to explain settings

**0.6.2**

* Default options did not work when updating plugin instead of installing
* Last bugfix broke pagination to work for multiple widgets of same type

**0.6.1**

* Increased theme compatibility

**0.6**

* Added option to change the horizontal alignment of the pagination
* Added German translation

**0.5.1**

* Bugfix: archive pagination did not work

**0.5**

* Added colorpicker

**0.4**

* New layout for options page
* Added styling options for pagination

**0.3**

* Improved styling of pagination
* Added defaults, notes and hints to options page
* Moved options page from Settings to Appearance in admin panel

**0.2**

* Set the number of pages to show
* Set the label of the previous and next link

**0.1**

* Chose the widget to be paginated
* Set the items per page individually

== Upgrade Notice ==