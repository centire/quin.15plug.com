<?php
/**
 * Uninstalls the Widget Paginator options when an uninstall has been
 * requested from the WordPress admin plugin dashboard
 *
 * see notes @http://codex.wordpress.org/Function_Reference/register_uninstall_hook
 */

delete_option('wgpag_options');
?>