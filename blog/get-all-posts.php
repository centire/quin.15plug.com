<?php

// Include blog header for wordpress function
require('wp-blog-header.php');

// Get post id
$postID = isset($_GET["postid"]) ? $_GET["postid"] : 0;

// Get number of post
$numberOfPost = isset($_GET["numberposts"]) ? $_GET["numberposts"]  : -1;

// Get category id
$categoryId = isset($_GET["catid"]) ? $_GET["catid"] : '';

// Get post type
$type = isset($_GET["type"])? $_GET["type"] : 'post'  ;

$returnPost = array();

if($postID) {
  $post = get_post( $postID);

	if($post) {
		$returnPost[] = [
			'id'      => $post->ID,
			'title'   =>  $post->post_title ,
			'content' =>  $post->post_content,
			'image'   => wp_get_attachment_url( get_post_thumbnail_id( $post->ID, 'single-post-thumbnail' ) )
		];
	}

} else {
	$args = array(

		'posts_per_page' => $numberOfPost,
		'offset'         => 0,
		'category'       => $categoryId,
		'orderby'        => 'date',
		'order'          => 'DESC',
		'post_type'      => $type,
		'post_status'    => 'publish',

	);

	$posts = get_posts( $args );

	if( $posts) {
		foreach ( $posts as $post ) {
			$returnPost[] = [
				'id'      => $post->ID,
				'title'   => $post->post_title ,
				'content' =>  $post->post_content ,
				'image'   => wp_get_attachment_url( get_post_thumbnail_id( $post->ID, 'thumbnail' ) )
			];
		}
	}

}

// Response return
echo json_encode($returnPost);