    jQuery(document).ready(function($){ 
     $(".show_album").click(function(e){
          var base_url = 'http://'+ document.location.hostname +'/';
          var data = $(this).attr('data-link');
          var title = $(this).attr('data-title');
          $.ajax({
               type: "POST",
               async : false,
               dataType: 'json',
               url: base_url +'za/public/vendor/vendor-portfolio/album-images/format/html',
               data: {id : data},
               beforeSend:function(){
                    initSpinnerFunction();
                    showSpinner();
               },
               complete:function( result ){
                    console.log(result.responseText);
                    if( result ){
                         fadeoutSpinner();
                         $('#tabs1 .album_det').fadeOut("slow",function() {
                              $('#tabs1 ul.isotope').hide();
                              $('#AlbumContainer').html( result.responseText );
                              bindFancyGallery();
                              $('#AlbumContainer').fadeIn("slow");
                              $('.album_content').removeClass("album_content");
                              $('.album_head_img').html( title ).fadeIn("slow");
                         });
                    return; 
                    }
               },
               error:function( error ){
                    return false;
               }
	  });
     });
     
     function initSpinnerFunction( TheLoadImg ) {
          var LoadingImg = TheLoadImg || true;
          ( $('#custom-Overlay').length > 0 )? $('#custom-Overlay').remove() : "" ;
          $("<div />").css({
               position: "fixed",
               display: "none",
               bottom:0,
               right:0,
               left: 0,
               top: 0,
               zIndex: 1000000,  // to be on the safe side
               "background-color": 'rgba(0,0,0,0.8)'
          }).attr('id','custom-Overlay').appendTo($("body"));
          
          if( LoadingImg!='false' )
               $("#custom-Overlay").append("<div style='position:absolute; left:50%;top:50%; margin-top:-64px;margin-left:-64px;' id ='PleaseWait' ><img src='"+base_url+"za/public/images/loading.gif' alt='plaese Wait' /></div>");
                     
     }
     
     function showSpinner(){         
          $('#custom-Overlay').show();
     }

     function fadeoutSpinner(){
             $('#custom-Overlay').fadeOut( "slow" );
     }

     function hideSpinner(){
             $('#custom-Overlay').hide();
     }
     
     (function ($, F) {
     F.transitions.resizeIn = function() {
          var previous = F.previous,
              current  = F.current,
              startPos = previous.wrap.stop(true).position(),
              endPos   = $.extend({opacity : 1}, current.pos);
  
          startPos.width  = previous.wrap.width();
          startPos.height = previous.wrap.height();
  
          previous.wrap.stop(true).trigger('onReset').remove();
  
          delete endPos.position;
  
          current.inner.hide();
  
          current.wrap.css(startPos).animate(endPos, {
              duration : current.nextSpeed,
              easing   : current.nextEasing,
              step     : F.transitions.step,
              complete : function() {
                  F._afterZoomIn();
  
                  current.inner.fadeIn("fast");
              }
          });
      };
  
      }(jQuery, jQuery.fancybox));
     
     function bindFancyGallery(){
	var imgSel = '.imagedetails';
	var galRel = 'media-gallery';
	jQuery( imgSel ).attr('rel', galRel ).fancybox({
		openEffect : 'elastic',
		openSpeed  : 150,
                

		closeEffect : 'elastic',
		closeSpeed  : 150,
		
		nextMethod : 'resizeIn',
		nextSpeed  : 250,
		
		prevMethod : false,
		type: 'iframe',
                iframe : {
                    scrolling : 'no'
                },
		autoScale:false,
		autoDimensions:false,
		afterShow: function() {
		}
	});
     }
     bindFancyGallery();
     });
