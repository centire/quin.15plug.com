var lastScrollTop = 0;
$(window).scroll(function(event){
	var topOffset = $('.top_bar').outerHeight(),
	windowScroll = $(window).scrollTop(),
	st = $(this).scrollTop();
	if (windowScroll > topOffset) {
		$('.nav_bar').addClass('sticky');
	} else if(windowScroll < topOffset) {
		$('.nav_bar').removeClass('sticky');
	}
	if($('.left_nav_links a').hasClass('active')) {
		if (st > lastScrollTop) {
			var nextElemId = $('.left_nav_links a.active').parent('li').next('li').find('a').attr('href');
			topPos = $(nextElemId).position().top,
			headerHeight = $('.sticky').outerHeight();
			if(topPos - windowScroll <= headerHeight) {
				$('.left_nav_links a').removeClass('active');
				$('.left_nav_links a[href=' + nextElemId + ']').addClass('active');
			}
		} else {
			if($('.left_nav_links li:not(:first-of-type) a').hasClass('active')) {
				var prevElemId = $('.left_nav_links li:not(:first-of-type) a.active').parent('li').prev('li').find('a').attr('href'),
				topPos2 = $(prevElemId).outerHeight() + $(prevElemId).position().top;
				if(topPos2 - windowScroll >= headerHeight + 50) {
					$('.left_nav_links a').removeClass('active');
					$('.left_nav_links a[href=' + prevElemId + ']').addClass('active');
				}
			}
		}
	} else {
		var firstElemId = $('.left_nav_links li:first-of-type a').attr('href');
		topPos = $(firstElemId).position().top,
		headerHeight = $('.sticky').outerHeight();
		if(topPos - windowScroll  <= headerHeight) {
			$('.left_nav_links a[href=' + firstElemId + ']').addClass('active');
		}
	}
	lastScrollTop = st;
});

$(window).load(function() {
	$('#banner').flexslider({
		directionNav:false
	});
	$('#blog_offer').flexslider({
		animation: "slide",
		animationLoop: false,
		minItems:1,
		itemWidth:320,
		maxItems:4,
		directionNav:false,
		itemMargin: 0
	});
	$('.features_slider').flexslider({
		animation: "slide",
		directionNav:false,
		controlNav:true
	});
});

$(document).on('click', '.select_rating li', function(){
	$('.select_rating li').removeClass('active');
	$(this).prevAll().addClass('active');
	$(this).addClass('active');
});

$(document).ready(function(){
	$('.block_title').each(function(){
		var that = $(this),
		thatText = that.text(),
		thatID = that.closest('.block').attr('id');
		thatHTML = '<li><a href="#' + thatID +'">' + thatText + '</a></li>'
		$('.left_nav_links').append(thatHTML);
	});
});

$(document).on('click', '.left_nav_links a', function(){
	var that = $(this),
	thatHref = that.attr('href'),
	idOffset = $(thatHref).offset().top,
	headerHeight = $('.sticky').outerHeight(),
	scrollAmount = idOffset - headerHeight;
	$('body, html').animate({
		scrollTop: scrollAmount
	}, 500);
	$('.left_nav_links a').removeClass('active');
	that.addClass('active');
	return false;
});

$(document).on('click', '.mobile_menu', function(){
	$('.main_nav').toggleClass('active');
	return false;
});