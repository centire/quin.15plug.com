jQuery(document).ready(function($){
    var xhr = null;
    $( ".item_1" ).click(function() {
        $( ".content_1" ).slideToggle();
    });
    
    $( ".item_2" ).click(function() {
        $( ".content_2" ).slideToggle();
    });
    
    $( ".item_3" ).click(function() {
        $( ".content_3" ).slideToggle();
    });

    function calculator() {
        // calculate total cost
        var totalActualCost = 0; // total actual cost 
        $('.actualCost').each(function() {
            totalActualCost += Number($(this).val());
        });
        
        // calculate total payment 
        var totalPayment = 0; // total payment
        $('.payment').each(function() {
            totalPayment += Number($(this).val());
        });
        
        // calculate balance due 
        var dueBalance = 0; // total payment
        $('.balanceDue').each(function() {
            dueBalance += Number($(this).val());
        });
        
        $("#totalActualCost").val(totalActualCost);
         totalPayment = totalPayment.toFixed(2);
        $("#totalPayment").val(totalPayment);
        var totalDuePayment = totalActualCost-totalPayment;
        totalDuePayment = totalDuePayment.toFixed(2);
        $("#totalDuePayment").val(totalDuePayment);
        
        // calculate super totals
        var a_c = $('#totalActualCost').val();
        var a_p = $('#totalPayment').val();
        var a_d_p = $('#totalDuePayment').val();
        $("span#totalBudget").text(a_c);
        $("span#totalPayments").text(a_p);
        $("span#totalDues").text(a_d_p);
        
        // calculate all values in service list [ payment, due ]
        $("table.budgetTable").find('tr.service_list').each(function(index){
            
            var due = $(this).find("input.actualCost").val()-$(this).find("input.payment").val();
            due = due.toFixed(2);
            $(this).find("input.balanceDue").val(due);
            
        });
        return true;
    }
    // call budget calculator by default
    calculator();
    
    var delay = (function(){
        var timer = 0;
        return function(callback, ms){
          clearTimeout (timer);
          timer = setTimeout(callback, ms);
        };
    })();
    
    // submit form on page load
    //$("form#budget_planner").ajaxForm({        
        //success: function() {            
            // trigger calculator
            $("table.budgetTable").find("input[type='text']").each(function(index){        
                $(this).keyup(function(){                    
                    delay(function(){
                        // check if values updated client side
                        if( calculator() ) {                        
                            $("form#budget_planner").ajaxForm({}).submit();
                        }
                    }, 2000 );
                });       
            });            
        //}        
    //}).submit();
    
    $(".budgetTable").find('tr.paynow div.makepayment').each(function(index){
        
        $(this).click(function(e){
            e.preventDefault();
            var id = $(this).attr('id');
            var booking = $(this).attr('booking');
            var load_paynow = '/users/dashboard/budgetpaynow/format/html?service_id='+id+'&booking_id='+booking;
            $("#create_account").load(load_paynow, function(){                                   
                $(this).lightbox_me({                                    
                    centered: true,
                    onLoad: function() { 
                        $(this).find('.form input:first').focus()
                        $(this).removeClass('hide');
                        //valiadate();
                        $('.close').click(function(){                                                
                            $(this).trigger('close');                                                
                        })
                        var existing =0;
                        //$("form#budget_planner").ajaxForm({}).submit();
                        $("#makepayment").click(function(e){ 
                            var bal_to_pay = $("#budgetPayment .amount_pay").val();
                            bal_to_pay = parseFloat(bal_to_pay.replace(",", ""));
                            var bal_due = parseFloat($("#service_list_"+id+" .balanceDue").val());
                            if(bal_to_pay>bal_due){
                                alert("Price Exceeding the Due Payment");
                                exit();
                            }
                            
                          var sele = $("select").val();
                            if (!sele) {
                                $("#preview").html("Select who is making the payment");
                                 exit();
                            }
                            //$("#service_list_"+id).find('input.payment').val($("#budgetPayment").find('input[name="amount"]').val());
                            $("#service_list_"+id).find('input.payment').each(function(index){                             
                                existing = Number($(this).val());
                                existing += Number($("#budgetPayment").find('input[name="amount"]').val().replace(",", ""));
                                $(this).val(existing);
                                if( calculator() ){
                                    $("form#budget_planner").ajaxForm({
                                        beforeSubmit:  function(){

                                                       
                                            $("form#budgetPayment").find('input[type="text"], select').each(function(event){
               
                                                if (!$(this).val()) {
                                                    $("#preview").html("All fields are required !!!");
                                                     exit();
                                                }                 
                                            });
                                            //alert("Validate form");    
                                        },
                                        success: function() {
                                            
                                           $("form#budgetPayment").ajaxForm({
                                                target: "#preview",
                                                beforeSubmit:  function(e){
                                                    var sele = $("select").val();
                                                    if (!sele) {
                                                            $("#preview").html("Select who is making the payment");
                                                             exit();
                                                        } 
                                                     $("form#budgetPayment").find('input[type="text"], select').each(function(event){
                                                         
                                                        if (!$(this).val()) {
                                                            $("#preview").html("All fields are required !!!");
                                                             exit();
                                                        }                 
                                                     });
                                                },
                                                success: function(msg) {
                                                    $("#preview").html(msg);   
                                                    setTimeout(function() {
                                                        location.reload();
                                                    }, 100);
                                                }    
                                                
                                            }).submit();
                                            
                                        }
                                    }).submit();                            
                                };                                
                            });                            
                        });          
                    },
                    closeClick: false,
                    appearEase: "swing",
                }); 
            });
            e.preventDefault();
            
        });
        
    });
    
    //enable tooltip on services name anchors
    $("[rel=tooltip]").tooltip({html:true});
    
    //add more service to budget planner
    $(".dash-content").find('div.addService').each(function(index){
        $(this).click(function(){
            var id = $(this).attr('id');
            $("form#budget_planner").ajaxForm({
                success: function() {
                    
                    $("#create_account").load('/users/dashboard/addnewservice/format/html?id='+id+'', function(){                                   
                        $(this).lightbox_me({                                    
                            centered: true,
                            onLoad: function() {
                                //if ( calculator() ) {
                                    //$("form#budget_planner").ajaxForm({                                    
                                        //success: function() {                                        
                                            $("#makepayment").click(function(e){
                                         
                                                $("form#addNewService").ajaxForm({
                                                    target: "#preview",
                                                    beforeSubmit:  function(){
                                                        $("form#addNewService").find('input[type="text"], select').each(function(index){
                                                            if (!$(this).val()) {
                                                                $("#preview").html("All fields are required !!!");
                                                                exit();
                                                            } 
                                                        });
                                                    },
                                                    success: function(msg) {
                                                        $("#preview").html(msg);
                                                        if(msg=="Success, you have add service to budget planner !!!") {
                                                            setTimeout(function() {
                                                                location.reload();
                                                            }, 100);
                                                        }
                                                    }                                                    
                                                }).submit();                           
                                            });                                        
                                        //}                                    
                                    //}).submit();
                                    
                                //} else {
                                    //$("#preview").html("Please Wait..... or try reloading the webpage");
                                //}
                            },
                            closeClick: false,
                            appearEase: "swing",
                        }); 
                    });
                    
                }
                
            }).submit();
            
        });
    });

/*---------------- Edit Payment 1 ------------------*/

 $(".editpayment").click(function(e){
            e.preventDefault();
            var id = $(this).attr('id');
            var booking = $(this).attr('booking');
            var data_id = $(this).attr('data-id');
            var load_paynow = '/users/dashboard/budgetpaynow/format/html?service_id='+id+'&booking_id='+booking+'&data_id='+data_id;
            $("#create_account").load(load_paynow, function(){                                   
                $(this).lightbox_me({                                    
                    centered: true,
                    onLoad: function() { 
                        $(this).find('.form input:first').focus()
                        $(this).removeClass('hide');
                        //valiadate();
                        $('.close').click(function(){                                                
                            $(this).trigger('close');                                                
                        })
                        var existing =0;
                        //$("form#budget_planner").ajaxForm({}).submit();
                        var bal_to_pay_old = $("#budgetPayment .amount_pay").val();
                        bal_to_pay_old = parseFloat(bal_to_pay_old.replace(",", ""));
                        $("#makepayment").click(function(e){
                            var bal_to_pay_new = $("#budgetPayment .amount_pay").val();
                            bal_to_pay_new = parseFloat(bal_to_pay_new.replace(",", ""));

                            var amount_payed = $("#service_list_"+id+" .payment").val();
                            amount_payed = parseFloat(amount_payed.replace(",", ""));
                            var actual_cost = parseFloat($("#service_list_"+id+" .actualCost").val());
                            var bal_to_made_extimate = amount_payed - bal_to_pay_old + bal_to_pay_new;
                            
                                if(bal_to_made_extimate>actual_cost ){
                                    alert("Price Exceeding Actual payment");
                                    exit();
                                }
                            
                            
                            var sele = $("select").val();
                            if (!sele) {
                                $("#preview").html("Select who is making the payment");
                                 exit();
                            }
                            //$("#service_list_"+id).find('input.payment').val($("#budgetPayment").find('input[name="amount"]').val());
                            $("#service_list_"+id).find('input.payment').each(function(index){                                
                               // existing = Number($(this).val());
                                //existing += Number($("#budgetPayment").find('input[name="amount"]').val());
                                //$(this).val(existing);
                                if( calculator() ){
                                    $("form#budget_planner").ajaxForm({
                                        beforeSubmit:  function(){
                                            //alert("Validate form");    
                                        },
                                        success: function() {
                                            
                                            $("form#budgetPayment").ajaxForm({
                                                target: "#preview",
                                                beforeSubmit:  function(){
                                                     $("form#budgetPayment").find('input[type="text"], select').each(function(index){                                       
                                                        if (!$(this).val()) {
                                                            $("#preview").html("All fields are required !!!");
                                                            exit();
                                                        }    
                                                    });
                                                },
                                                success: function(msg) {
                                                    $("#preview").html(msg); 
                                                    setTimeout(function() {
                                                        location.reload();
                                                    }, 100);
                                                }    
                                                
                                            }).submit();
                                            
                                        }
                                    }).submit();                            
                               };                                
                            });                            
                        });       
                    },
                    closeClick: false,
                    appearEase: "swing",
                }); 
            });
            e.preventDefault();
            
        });
    
/*---------------- Edit Payment 2 ------------------*/
      /*$(".editpayment").click(function(e){
            e.preventDefault();
            var id = $(this).attr('id');
            var booking = $(this).attr('booking');
            var data_id = $(this).attr('data-id');
            var load_paynow = '/users/dashboard/budgetpaynow/format/html?service_id='+id+'&booking_id='+booking+'&data_id='+data_id;
            $("#create_account").load(load_paynow, function(){                                   
                $(this).lightbox_me({                                    
                    centered: true,
                    onLoad: function() { 
                        $(this).find('.form input:first').focus()
                        $(this).removeClass('hide');
                        //valiadate();
                        $('.close').click(function(){                                                
                            $(this).trigger('close');                                                
                        })
                        var existing =0;
                        //$("form#budget_planner").ajaxForm({}).submit();
                        $("#makepayment").click(function(e){
                            
                            //$("#service_list_"+id).find('input.payment').val($("#budgetPayment").find('input[name="amount"]').val());
                            $("#service_list_"+id).find('input.payment').each(function(index){                                
                                existing = Number($(this).val());
                                existing += Number($("#budgetPayment").find('input[name="amount"]').val());
                                $(this).val(existing);
                                if( calculator() ){
                                    $("form#budget_planner").ajaxForm({
                                        beforeSubmit:  function(){
                                            //alert("Validate form");    
                                        },
                                        success: function() {
                                            
                                            $("form#budgetPayment").ajaxForm({
                                                target: "#preview",
                                                beforeSubmit:  function(){
                                                     $("form#budgetPayment").find('input[type="text"], select').each(function(index){
                                                        
                                                        if (!$(this).val()) {
                                                            $("#preview").html("All fields are required !!!");
                                                            exit();
                                                        } else {   }
                                                        
                                                    });
                                                },
                                                success: function(msg) {
                                                    $("#preview").html(msg);   
                                                }    
                                                
                                            }).submit();
                                            
                                        }
                                    }).submit();                            
                                };                                
                            });                            
                        });          
                    },
                    closeClick: false,
                    appearEase: "swing",
                }); 
            });
            e.preventDefault();
            
        });*/

});