    jQuery(document).ready(function($){
	jQuery.fn.center = function () {
	    this.css("position","absolute");
	    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + 
		$(window).scrollTop()) + "px");
	    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + 
		$(window).scrollLeft()) + "px");
	    return this;
	}
        var base_url = 'http://'+ document.location.hostname +'/';
        bindFollowToBtn();
        
        $('.menu_like').click(function(){
            $(this).parent().children('.click_bar_show').fadeToggle();
        });
        $(".image_fance_popup").mouseleave(function(){
            $(".click_bar_show").fadeOut(1000);
        });
        $('.fbs_share').click(function(){
            $('.share_options').fadeToggle();
        });
        $('.wrapper.position-relative > ul > li').mouseleave(function(){
            $('.share_options').fadeOut();
        });
        
        var followBtnClicked;
        
        function bindFollowToBtn(){
            $('.follow_btn').unbind("click");
            $('.follow_btn').click(function(){
                var $this = $( this );
                followBtnClicked= $( this );
                var imageId = $this.attr('image-data');
                if(typeof imageId === "undefined") {
                    return false;
                }
                var postData = { imageId : imageId , tyepe : 'image' };
                $.ajax({
                    type: "POST",
                    async : false,
                    dataType: 'json',
                    url: base_url+'za/public/vendor/image/make-me-follow',
                    data: postData,
                    beforeSend:function(){
                        initSpinnerFunction();
                        showSpinner();
                    },
                    complete:function( result ){
                        console.log(result.responseText);
                        if( result.responseText == 'needToLogin' ){
                           fadeoutSpinner();
                           openIPopUp();
                        }
                    },
                    error:function( error ){
                        return false;
                    }
                });
            });
        }
        function initSpinnerFunction( TheLoadImg ) {
            var LoadingImg = TheLoadImg || true;
            ( $('#custom-Overlay').length > 0 )? $('#custom-Overlay').remove() : "" ;
            $("<div />").css({
                position: "fixed",
                display: "none",
                bottom:0,
                right:0,
                left: 0,
                top: 0,
                zIndex: 1000000,  // to be on the safe side
                "background-color": 'rgba(0,0,0,0.8)'
            }).attr('id','custom-Overlay').appendTo($("body"));
            
            if( LoadingImg!='false' )
               $("#custom-Overlay").append("<div style='position:absolute; left:50%;top:50%; margin-top:-64px;margin-left:-64px;' id ='PleaseWait' ><img src='"+base_url+"images/loading.gif' alt='plaese Wait' /></div>");
                     
        }
        
        function showSpinner(){         
          $('#custom-Overlay').show();
        }

        function fadeoutSpinner(){
            $('#custom-Overlay').fadeOut( "slow" );
        }

        function hideSpinner(){
            $('#custom-Overlay').hide();
        }
	
 
        function openIPopUp(){
            $("<div />").css({
                position: "absolute",
                left: 0,
                top: 0,
                width: $(document).width(), // width same as that of document
                height: $(document).height(), // height same as that of document
                zIndex: 1000000,  // to be on the safe side
                "background-color": 'rgba(0,0,0,0.8)'
            }).attr('id','custom-Overlay-pop').appendTo($("body"));
            var TheId =$.now();
            $('<div />', {
                id: TheId,
                height:290,
                width:599
            }).appendTo('#custom-Overlay-pop').addClass("outer-Iframe").css({  padding: '20px' ,'background-color' : '#fff',display:'block',zIndex: 1000001 });
	    //alert($(document).width());
            if( $(document).width() < 580){
                var divWidt = $(document).width()-10;
                var divHigt = divWidt * 0.75;
            }else{
                var divWidt = 557;
                var divHigt = 250;
            }
   
            $('<iframe />', {
                id: TheId+'-iframe',
                src: base_url+'za/public/vendor/iframe',
                height:divHigt,
		width:divWidt
            }).appendTo('#'+TheId).css({ 'background-color' : '#fff', 'border': '1px solid #ddd' });
            $('#'+TheId).center();
            $('#'+TheId).prepend( '<a class="popup_cancel" style="background-image: url(../images/kross.png); background-repeat:no-repeat; height: 25px;position: absolute; right: -11px;top: -12px;width: 24px;z-index: 8040;" onclick="closePopUp(\''+TheId+'\');" href="javascript:void(0);"></a>' );
        }
        
        $('#showMailFields').click(function(e){
            e.preventDefault();
            var cssTop = parseInt($( this ).offset().top) + 65;
            var cssleft = $( this ).offset().left;
            $('#attachment_mail').css({ 'top' : cssTop,'left' : cssleft, 'z-index' : '999999999' }).slideDown();
            $('#sendImageMailForm').show();
            $('#attachment_mail').children('[type=email]').focus();
	});


    
    });
    
    function closePopUp( popupid ){ 
	$('#'+popupid).css({ display:'none',zIndex: 0});
	if($('#'+popupid).children('iframe').length >0 )
	{
	     $('#'+popupid).children('iframe').remove();
	}
	$('#custom-Overlay-pop').remove();
    }
    
    function hidemailDiv(){
	$('#attachment_mail').fadeOut('fast');
    }
