


IndexApp.filter('startFrom', function () {
	return function (input, start) {
		if (input) {
		start = +start;
		return input.slice(start);
		}
		return [];
	};
}); 

IndexApp.controller("vendorSearchIndex",['$scope', '$http','filterFilter',function($scope,$http){




  var process = true;
  $scope.search = {};
  $scope.search.search_key = '';
  $scope.search.by_location ='';
  $scope.search.rating = 0;
  $scope.search.category = [];
  $scope.searchResult ={};
  $http.get("/vendor/search/getcategory").success(function(response) {
   $scope.searchResult = response;
   var loc_defalut = {name:"-- Select All --",value:""}; // code for adding default value in location
   $scope.searchResult.location.unshift(loc_defalut);// for all the searchs
   $scope.vendorResult = response.vendor;

   	// pagination controls
$scope.currentPage = 1;
$scope.totalItems = response.vendor.length;
$scope.entryLimit = 4; // items per page
$scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit); 

	// $watch search to update pagination
$scope.$watch('search', function (newVal, oldVal) {
	
	//$scope.filtered = filterFilter($scope.vendorResult, newVal);
	var value = "; " + document.cookie;
	var parts = value.split("; key1=");
  if (parts.length == 2) 
  	var afterfilterval = parts.pop().split(";").shift();

	$scope.totalItems = afterfilterval;
	$scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
	$scope.currentPage = 1;
	//alert("dss"+document.cookie)
}, true); 

  });







   	$scope.login =function(){
   		$( "#login" ).trigger( "click" );
   	}

   	$scope.resest_filter = function(){
   		  $scope.search.search_key = '';
		  $scope.search.by_location ='';
		  $scope.search.rating = 0;
		  $scope.search.category = [];
   	}

    $scope.callback_quote =function(param){
  		 var dateToday = new Date();
		 $('.appointmentDate').datepicker('setDate', null);
		 $(".appointmentDate").datepicker("destroy");
		 $('.hasDatePickerWithCal').datepicker('setDate', null);
		 $(".hasDatePickerWithCal").datepicker("destroy");
	     	$(".appointmentDate").datepicker({
			dateFormat: 'MM dd, yy',
			minDate: dateToday,
			minDate: 0,
			changeMonth: true,
			changeYear: true,
			onSelect: function(selectedDate) {
			    $(this).next().val(selectedDate);
			} 
		});

		$( ".hasDatePickerWithCal" ).datepicker({
			showOn: "button",
			buttonImage: '/images/calender_icon.png',
			buttonImageOnly: true,
			dateFormat: 'MM dd, yy',
			changeMonth: true,
			changeYear: true,

			beforeShow: function (input, inst) {

				var changedate = 0;

			   $(".hasDatePickerWithCal").datepicker("option", { minDate:changedate });
			}
		});
		var id = "call_"+param; 
		var key = $("#"+id).attr('data-check');
		$("#rqstipt"+key).hide();
		$("#cellno"+key).show();
		$("#radio4_"+key).prop('checked', true);
		$("#"+id).find('.celno').attr('required', true);
		$("#"+id).find('.stepExample1').removeAttr('required');
		$("#"+id).find('.stepExample2').removeAttr('required');
		$("#"+id).find('.appointmentDate').removeAttr('required');
		$("#"+id).show("slow");
    }
    
    $scope.request_appointment = function(param){

				  var dateToday = new Date();
				 $('.appointmentDate').datepicker('setDate', null);
				 $(".appointmentDate").datepicker("destroy");
				 $('.hasDatePickerWithCal').datepicker('setDate', null);
				 $(".hasDatePickerWithCal").datepicker("destroy");
			     	$(".appointmentDate").datepicker({
					dateFormat: 'MM dd, yy',
					minDate: dateToday,
					minDate: 0,
					changeMonth: true,
					changeYear: true,
					onSelect: function(selectedDate) {
					    $(this).next().val(selectedDate);
					}
				});

				$( ".hasDatePickerWithCal" ).datepicker({
					showOn: "button",
					buttonImage: '/images/calender_icon.png',
					buttonImageOnly: true,
					dateFormat: 'MM dd, yy',
					changeMonth: true,
					changeYear: true,
					beforeShow: function (input, inst) {
					    var dd = $(this).parent().parent().parent().find("input[name=date-com]").val();
					    if(dd){
					    	var changedate = dd ;
					   }else{
						var changedate = 0;
					   }
					    $(".hasDatePickerWithCal").datepicker("option", { minDate: changedate });
					}
				});
			var id = "call_"+param; 
			var key = $("#"+id).attr('data-check');
			$("#rqstipt"+key).show();
			$("#cellno"+key).hide();
			$("#radio5_"+key).prop('checked', true);
			$("#"+id).find('.celno').removeAttr('required');
			$("#"+id).find('.stepExample1').attr('required', true);
			$("#"+id).find('.stepExample2').attr('required', true);
			$("#"+id).find('.appointmentDate').attr('required', true);
			$("#"+id).show("slow");
    }

 
    $scope.radio_radiog_dark = function(parms,user_id){

    	if (parms == "quote") {
		$('.hasDatePickerWithCal').datepicker('setDate', null);
		$(".hasDatePickerWithCal").datepicker("destroy");                  
	        $( ".hasDatePickerWithCal" ).datepicker({
	                showOn: "button",
	                buttonImage: '/images/calender_icon.png',
	                buttonImageOnly: true,
	                dateFormat: 'MM dd, yy',
	                changeMonth: true,
	                changeYear: true,

			beforeShow: function (input, inst) {
			   var changedate = 0;
	                   $(".hasDatePickerWithCal").datepicker("option", { minDate:changedate });
	                }
		});
	            var Cl1 =  'cellno'+user_id; //$(e.target).next().closest( "div" ).next().attr('id');
	            var hid = 'rqstipt'+user_id;//$(e.target).parent().parent().next().find('.rqstipt-width').attr('id');
	            $("#"+Cl1).show();
	            $("#"+hid).hide();
	        } 
	        else {
		
		     $('.hasDatePickerWithCal').datepicker('setDate', null);
		     $(".hasDatePickerWithCal").datepicker("destroy");
			 $( ".hasDatePickerWithCal" ).datepicker({
			        showOn: "button",
			        buttonImage: '/images/calender_icon.png',
			        buttonImageOnly: true,
			        dateFormat: 'MM dd, yy',
			        changeMonth: true,
			        changeYear: true,

				beforeShow: function (input, inst) {
				    var dd = $(this).parent().parent().parent().find("input[name=date-com]").val();
				    if(dd){
				    	var changedate = dd ;
				   }else{
					var changedate = 0;
				   }
			           $(".hasDatePickerWithCal").datepicker("option", { minDate:changedate });
			        }
			 });
			 $('.appointmentDate').datepicker('setDate', null);
		         $(".appointmentDate").datepicker("destroy");
		        var dateToday = new Date();
	     		$(".appointmentDate").datepicker({
				dateFormat: 'MM dd, yy',
				minDate: dateToday,
				minDate: 0,
				changeMonth: true,
				changeYear: true,
				onSelect: function(selectedDate) {
			            $(this).next().val(selectedDate);
			        }
			});   
	            var Cl = 'cellno'+user_id; //$(e.target).next().closest( "div" ).next().attr('id');
	            var hid1 = 'rqstipt'+user_id; //$(e.target).parent().parent().parent().find('.cell').attr('id');
	            $("#"+Cl).show();
	            $("#"+hid1).hide();
	        } 
    }

    $scope.close_sld_cntn = function(parms){
    	$("#call_"+parms).hide("slow");
    }

    $scope.request_favorite = function(parms){

	     var id = 'call_'+parms; //$(e.target).attr('data-key');
        var vendor = $("#"+id).find("input[name=vendor_member]").val();
        if($("#fav_id_"+parms).hasClass('favourite-icon') ) {
            var like = 0; 
        }else{
            var like = 1;
        }
        $.ajax({
            type: 'POST',
            url : '/users/operations/favourite/format/html',
            data: { vendor_member: vendor, terms: 'favorite', check : like }
        }).done(function(result) {
            if(result.image == 'add'){
                $("#fav_id_"+parms).addClass( 'favourite-icon' );
            }else {
                $("#fav_id_"+parms).removeClass( 'favourite-icon' );
            }
        });

    }

    $scope.request_like = function(parms){
    	like_fun(parms);
    }

    function like_fun(parms){
    	   var id = parms; //$(e.target).attr('data-key');
        var cl = 'call_'+parms; //$(e.target).attr('id');
        var vendor = parms; //$("#"+id).find("input[name=vendor_member]").val();
        $("#target").load('/users/operations/likes/format/html?id='+ id + '&vendor='+ vendor , function(){   //load data from given location
            $(this).lightbox_me({
                centered: true,
                onLoad: function() {   
                    $('.close').click(function() {                        
                        $(this).trigger('close');    //event ->close                        
                         $(".rating-content").remove();
                    })                 
                    var that = this;
                    $('#vendor'+id).bootstrapValidator({
                        submitHandler: function(validator, form, submitButton) {
                            var star = $(form).find("input[name=star_rate]").val();
                            if ( star ) {
                            }else{
                                alert("Please give rating to this vendor.")
                                $(submitButton).prop("disabled", false);
                                return;    
                            }
                            
                            var opts = {
                                success: function(data) {
                                   $("#"+cl).html('Like [ '+ data.count +' ]');
                                   $(".lb_overlay").remove();
                                   $(".rate-model").hide('slow');                         
                                   $(".rating-content").remove();
                                   //$scope.rating = star;
                                }
                            };
                            form.ajaxSubmit(opts);
                            $http.get("/vendor/search/getcategory").success(function(response) {
							   $rootScope.searchResult = response;
							   $scope.vendorResult = response.vendor;
							});
                        }                   
                    });
                    
                },
                closeClick: false,
                appearEase: "swing",                
            }); 
        });
      
    }

        function submit_search_form() {
              
	                if ( process ) {
	                    process = false;
	                    $('.search-content').html("loading data....wait").promise().done(function(){
	                        $("#vendor_search").ajaxForm({
	                            target: ".search-content",
	                            success: function(data){
	                                
					   //@ Dinesh request to show Quotes form
					    $("a.callback_quote").click(function(e){
					       var dateToday = new Date();
						 $('.appointmentDate').datepicker('setDate', null);
						 $(".appointmentDate").datepicker("destroy");
						 $('.hasDatePickerWithCal').datepicker('setDate', null);
						 $(".hasDatePickerWithCal").datepicker("destroy");
					     	$(".appointmentDate").datepicker({
							dateFormat: 'MM dd, yy',
							minDate: dateToday,
							minDate: 0,
							changeMonth: true,
							changeYear: true,
							onSelect: function(selectedDate) {
							    $(this).next().val(selectedDate);
							} 
						});
			
						$( ".hasDatePickerWithCal" ).datepicker({
							showOn: "button",
							buttonImage: '/images/calender_icon.png',
							buttonImageOnly: true,
							dateFormat: 'MM dd, yy',
							changeMonth: true,
							changeYear: true,

							beforeShow: function (input, inst) {
				
								var changedate = 0;

							   $(".hasDatePickerWithCal").datepicker("option", { minDate:changedate });
							}
						});
					 	e.preventDefault(); //to prevent same popup appear multiple times on  multiple clicks        
						var id = $(e.target).attr('data-key');
						var key = $("#"+id).attr('data-check');
						$("#rqstipt"+key).hide();
						$("#cellno"+key).show();
						$("#radio4_"+key).prop('checked', true);
						$("#"+id).find('.celno').attr('required', true);
						$("#"+id).find('.stepExample1').removeAttr('required');
						$("#"+id).find('.stepExample2').removeAttr('required');
						$("#"+id).find('.appointmentDate').removeAttr('required');
						$("#"+id).show("slow");
					    
					    });
					    
					    //@ jeevan request handler for appointments with public vendors
					    $("a.request_appointment").click(function(e){
						       e.preventDefault(); //to prevent same popup appear multiple times on  multiple clicks
							  var dateToday = new Date();
							 $('.appointmentDate').datepicker('setDate', null);
							 $(".appointmentDate").datepicker("destroy");
							 $('.hasDatePickerWithCal').datepicker('setDate', null);
							 $(".hasDatePickerWithCal").datepicker("destroy");
						     	$(".appointmentDate").datepicker({
								dateFormat: 'MM dd, yy',
								minDate: dateToday,
								minDate: 0,
								changeMonth: true,
								changeYear: true,
								onSelect: function(selectedDate) {
								    $(this).next().val(selectedDate);
								}
							});
			
							$( ".hasDatePickerWithCal" ).datepicker({
								showOn: "button",
								buttonImage: '/images/calender_icon.png',
								buttonImageOnly: true,
								dateFormat: 'MM dd, yy',
								changeMonth: true,
								changeYear: true,
								beforeShow: function (input, inst) {
								    var dd = $(this).parent().parent().parent().find("input[name=date-com]").val();
								    if(dd){
								    	var changedate = dd ;
								   }else{
									var changedate = 0;
								   }
								    $(".hasDatePickerWithCal").datepicker("option", { minDate: changedate });
								}
							});
						var id = $(e.target).attr('data-key');
						var key = $("#"+id).attr('data-check');
						$("#rqstipt"+key).show();
						$("#cellno"+key).hide();
						$("#radio5_"+key).prop('checked', true);
						$("#"+id).find('.celno').removeAttr('required');
						$("#"+id).find('.stepExample1').attr('required', true);
						$("#"+id).find('.stepExample2').attr('required', true);
						$("#"+id).find('.appointmentDate').attr('required', true);
						$("#"+id).show("slow");
					    });

				    $('input:radio[name="radiog_dark"]').change(function(e){
					        e.preventDefault();
					    if ($(this).val() == "quote") {
						$('.hasDatePickerWithCal').datepicker('setDate', null);
						$(".hasDatePickerWithCal").datepicker("destroy");                  
					        $( ".hasDatePickerWithCal" ).datepicker({
					                showOn: "button",
					                buttonImage: '/images/calender_icon.png',
					                buttonImageOnly: true,
					                dateFormat: 'MM dd, yy',
					                changeMonth: true,
					                changeYear: true,

							beforeShow: function (input, inst) {
							   var changedate = 0;
					                   $(".hasDatePickerWithCal").datepicker("option", { minDate:changedate });
					                }
						});
					            var Cl1 = $(e.target).next().closest( "div" ).next().attr('id');
					            var hid = $(e.target).parent().parent().next().find('.rqstipt-width').attr('id');
					            $("#"+Cl1).show();
					            $("#"+hid).hide();
					        } 
					        else {
						
						     $('.hasDatePickerWithCal').datepicker('setDate', null);
						     $(".hasDatePickerWithCal").datepicker("destroy");
							$( ".hasDatePickerWithCal" ).datepicker({
							        showOn: "button",
							        buttonImage: '/images/calender_icon.png',
							        buttonImageOnly: true,
							        dateFormat: 'MM dd, yy',
							        changeMonth: true,
							        changeYear: true,

								beforeShow: function (input, inst) {
								    var dd = $(this).parent().parent().parent().find("input[name=date-com]").val();
								    if(dd){
								    	var changedate = dd ;
								   }else{
									var changedate = 0;
								   }
							           $(".hasDatePickerWithCal").datepicker("option", { minDate:changedate });
							        }
							});
							 $('.appointmentDate').datepicker('setDate', null);
						         $(".appointmentDate").datepicker("destroy");
						        var dateToday = new Date();
					     		$(".appointmentDate").datepicker({
								dateFormat: 'MM dd, yy',
								minDate: dateToday,
								minDate: 0,
								changeMonth: true,
								changeYear: true,
								onSelect: function(selectedDate) {
							            $(this).next().val(selectedDate);
							        }
							});   
					            var Cl = $(e.target).next().closest( "div" ).next().attr('id');
					            var hid1 = $(e.target).parent().parent().parent().find('.cell').attr('id');
					            $("#"+Cl).show();
					            $("#"+hid1).hide();
					        } 
				    });    
					process = true;
	                            }    
	                        }).submit(); 
	                    });
	                    
	                }        
        }
}]);


IndexApp.filter('vendorFilter', function() {

  return function(vendors,search_key,rating,by_location,category) {


    var filtered = [];
    var search_key = new RegExp(search_key, 'i');
    var rating = new RegExp(rating, 'i');
    var by_location = new RegExp(by_location, 'i');
    if(typeof vendors != 'undefined'){
	    for (var i = 0; i < vendors.length; i++) {
	    	var vendor = vendors[i];
	    	if((search_key.test(vendors[i].first_name) || search_key.test(vendors[i].last_name) || search_key.test(vendors[i].address) || search_key.test(vendors[i].about) || search_key.test(vendors[i].zipcode) || search_key.test(vendors[i].company_name) ) && rating.test(vendors[i].rating) && by_location.test(vendors[i].location) ){
	    		if(category.length==0){
	    			filtered.push(vendor);
	    		}else{
	    			for(var j=0 ;j<category.length;j++){
	    				if(category[j]==vendors[i].category){
	    					filtered.push(vendor);
	    				}
	    			}
	    		}
	    	}
	    }
	}
	document.cookie = "key1="+filtered.length;
    return filtered;
    

  }

});

