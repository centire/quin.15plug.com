  (function() {
    /****
     * @quinceanera handle user message & notifications
     * requests
     */
    VendorApp.controller('MessnotificationController', [
      '$scope', '$http', '$interval', function($scope, $http, $interval){
        // Simple GET request example :
        // calling notification on refresh
        notifications();
        // calling notification in half minutes interval
        $interval(notifications, 30000);
        function notifications() {        
          $http.get('/notice/index/notify/format/html').
            success(function(data, status, headers, config) {
              $scope.data = data;
              $scope.total = $scope.data.messageNotifications.length+$scope.data.systemNotifications.length              
            }).
            error(function(data, status, headers, config) {
              
            });
        }
      }  
    ]);
    VendorApp.directive('orientable', function () {       
        return {
            link: function(scope, element, attrs) {
                element.bind("load" , function(e){    
                    // success, "onload" catched
                    // now we can do specific stuff:
                    this.className = "vertical";
                });
            }
        }
    });
  }).call(this);