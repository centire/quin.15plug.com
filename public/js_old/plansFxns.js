/** @ Author: Hitesh Thakur **/
/** @ Date: 12 Feb 2014 **/
/** @ File functionality: Save vendor plan  **/
/** @ Description: reusable code for vendor plan change via ajax  **/

// get value of selected plan on page load
var planValue = getPlanValue();
 
// hide image, show radio buttons, change button/link text
function showPlanRadios( btn, btnTxt, fxnName ){
	planValue = getPlanValue();
	var btnTxt =  btnTxt || "Save plan";
	var fxnName = fxnName || "savePlan";
	
	if($(btn).is("input")){
		$(btn).val( btnTxt ).attr("onclick","javascript:"+fxnName+"( 'this' );");
	}else{
		$(btn).html( btnTxt ).attr("onclick","javascript:"+fxnName+"( 'this' );");
	}
		
	jQuery("img.plans.selected").hide();
	jQuery(".plans.radio").each(function(i,v){
		jQuery(this).show();
	});
}

// show image, hide radio buttons, change button/link text and call save plans fxn
function savePlan( btn, btnTxt, fxnName ,callSavePlansNow){
	var btnTxt =  btnTxt || "Change plan";
	var fxnName = fxnName || "showPlanRadios";
	var callSavePlansNow= callSavePlansNow || true;

	if($(btn).is("input")){
		$(btn).val( btnTxt ).attr("onclick","javascript:"+fxnName+"( 'this' );");
	}else{
		$(btn).html( btnTxt ).attr("onclick","javascript:"+fxnName+"( 'this' );");
	}
	
	jQuery("img.plans.selected").show();
	jQuery(".plans.radio").each(function(i,v){
		jQuery(this).hide();
	});
	
	if(callSavePlansNow){
		savePlansNow();
	}
}

// actually saving the plan via ajax call
function savePlansNow(){

	if(planValue==getPlanValue()){
		showErrorHide( 'custom-Overlay', 'PleaseWait', ' Sorry, But no changes were made in Plan !!' );
		return false;
	}else{
		var planData = { plan: getPlanValue() };
	}
	
	$.ajax({
		type:"post",
		url:base_url+'vendors/savePlanChangeRequest',
		data: planData ,
		dataType:'html',
		beforeSend:function(){
			showSpinner();
		},
		complete:function( result ){
				if( result.responseText =='success' ){
					fadeoutSpinner();
                                          window.location = base_url+"Vendors/billing";
					return true;
				 }else{
					showErrorHide();
					return false;
				}
		},
		error:function( error ){
			showErrorHide();
			return false;
		}
	});
}

// get the current value from plan type radio buttons 
function getPlanValue( planSelector ){

	var planSelector = planSelector || ".plans.radio";
	
	var planRadioButtons = jQuery( planSelector ); 
	var ThePlanvalue ='';

	if(planRadioButtons.length > 0){
	
		planRadioButtons.each(function(i,v){

			if(jQuery(this).is(":checked")){
				ThePlanvalue = jQuery(this).val();
			}
			
		});
                return ThePlanvalue ;
	}
}


// Cancel prior requests for plan change made by vendor
function cancelPlanChangeRequests(){
		$.ajax({
		type:"post",
		url:base_url+'vendors/cancelPlanRequests',
		data: {} ,
		dataType:'html',
		beforeSend:function(){
			showSpinner();
		},
		complete:function( result ){
				if( result.responseText =='success' ){
					fadeoutSpinner();
                                        window.location = base_url+"Vendors/billing";
					return true;
				 }else{
					showErrorHide( 'custom-Overlay', 'PleaseWait', ' Sorry, But not able to cancel plan Change request please try again later !!' );;
					return false;
				}
		},
		error:function( error ){
			showErrorHide( 'custom-Overlay', 'PleaseWait', ' Sorry, But not able to cancel plan Change request please try again later !!' );;
			return false;
		}
	});
}